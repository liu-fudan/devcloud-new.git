# 研发云平台

+ 用户中心 (基于ns的RABC): mcenter
  + 将公用的能力(认证/鉴权)从各服务中拆除，直接通过mcenter统一化管理。
  + 登录后返回token，用户携带token访问业务服务(cmdb,mpass...)，业务服务通过GRPC访问mcenter校验token
+ 审计中心 (记录用户行为，避免纠纷): maudit

+ 资源中心: 运维体系里面关联基础资源(ECS, DB): cmdb
+ 发布中心: 应用的全生命周期(基于k8s的CI/CD): mpaas

## 技术
2套接口:
  + RESTful接口: 用于Web
    + Go Restful: 专门用于开发Restful API, K8s的API Server 就是用这个框架开发。相比于Gin多了路由装饰的能力
  + 内部接口: GRPC, 用户认证中间件
  + 数据存储:
    + MongoDB：保存非结构化数据
    + Mysql:保存结构化数据，即资源中心的数据