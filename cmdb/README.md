# CMDB
## 需求
场景: 资源管理
原先可能用excel维护资源和应用的关系
现在通过cmdb管理 主机、IP、证书、中间件服务、LB、...
## 核心功能
1. 资源录入：腾讯云、百度云等都有官方的SDK，我们封装成provider
2. 资源关系维护
3. 资源检索
   + 属性检索(模糊): +通用属性: 直接通过meta表进行过滤,LIKE。独有属性: 不推荐
## 设计方案  
1. 关系维护与基于tag解索:
   + tag: tag表, 用于关联资源meta.id
2. 资源间关系: LB--->A(host)--->ENI--->IP
   meta:
    LB(id:lb01)
    A(id:host01)
    ENI(id:eni01)
    IP(id:ip01)
   relation_ship
    children parent
    ip01     eni01
    eni01    host01
    host01   lb01
## 业务设计
### resource 资源通用属性
meta：资源静态数据，创建后不允许修改。如id，domain，namespace等
spec：资源动态属性，如厂商，资源类型，region，owner
cost：费用信息，如付费方式，单价
status：状态信息
tags: 标签，分为系统标签、第三方标签。
contentHash：资源内容的hash，更新时先比对hash

PUT和SEARCH操作。
PUT支持更新和创建。resource里的属性存放在多个表里，每个表主键都为resource_id。更新时启动事务，执行多个对象的保存。先makeContentHash，对比后再执行相应的保存操作。云商的标签不能全量保存，先清空，再保存。
SEARCH，先连表，tag由于是多对1，需要添加遍历添加where语句。
### provider
每个厂商封装一个。secretId,secretKey,资源转换完成后的hook，令牌桶限流。
### secret 资源同步凭证
provider，服务地址，api_key,api_secret
创建凭证，查询凭证，syncResource(资源同步):拿着凭证ID找到对应的provider，创建client，设置回调函数(hook)，调用sync 

#### 腾讯云
云服务器，南京1区，20G
账号头像，访问管理
子账号test 
SecretId:AKIDn2VDZtJoLrV9qXl4Nh2TtGfXND459CDB
SecretKey:Y4a7d9lNz146EOZm0DnQEMf8dAh8YuGc