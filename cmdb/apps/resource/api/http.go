package api

import (
	"gitee.com/liu-fudan/devcloud-new/cmdb/apps/resource"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
)

type handler struct {
	service resource.Service
}

var (
	h = &handler{}
)

func (h *handler) Config() error {
	h.service = app.GetInternalApp(resource.AppName).(resource.Service)
	return nil
}

func (h *handler) Name() string {
	return resource.AppName
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{"资产"}
	ws.Route(ws.POST("/").To(h.SearchResource).
		Doc("查询资产").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Metadata("service", "corjuu2mpgsn6g1b3h50").
		Metadata("auth", true).
		Metadata("perm", true).
		Reads(resource.SearchRequest{}).
		Writes(resource.ResourceSet{}).
		Returns(200, "OK", resource.ResourceSet{}))

	ws.Route(ws.POST("/put").To(h.PutResource).
		Doc("更新资产").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Metadata("service", "corjuu2mpgsn6g1b3h50").
		Metadata("auth", true).
		Metadata("perm", true).
		Reads(resource.NewResource()).
		Writes(resource.NewResource()).
		Returns(200, "OK", resource.NewResource()))
}

func init() {
	app.RegistryRESTfulApp(h)
}
