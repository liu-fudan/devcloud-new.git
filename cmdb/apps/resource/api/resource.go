package api

import (
	"gitee.com/liu-fudan/devcloud-new/cmdb/apps/resource"
	"gitee.com/liu-fudan/devcloud-new/cmdb/common/errors"
	"github.com/emicklei/go-restful/v3"
)

func (h *handler) SearchResource(r *restful.Request, w *restful.Response) {
	in := resource.NewSearchRequest()
	err := r.ReadEntity(in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}
	user, err := h.service.Search(r.Request.Context(), in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}

	w.WriteEntity(user)
}

func (h *handler) PutResource(r *restful.Request, w *restful.Response) {
	in := resource.NewResource()
	err := r.ReadEntity(in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}
	user, err := h.service.Put(r.Request.Context(), in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}

	w.WriteEntity(user)
}
