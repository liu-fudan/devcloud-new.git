package impl

import "gitee.com/liu-fudan/devcloud-new/cmdb/apps/resource"

// resource_meta
type ResourceMeta struct {
	*resource.Meta
	*resource.ContentHash
}
