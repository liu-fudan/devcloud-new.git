package impl_test

import (
	"context"

	"gitee.com/liu-fudan/devcloud-new/cmdb/apps/resource"
	"gitee.com/liu-fudan/devcloud-new/cmdb/test/tools"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
)

var (
	// 声明需要测试的对象
	Impl resource.Service

	ctx = context.Background()
)

// 加载ioc, 需要加载测试用例的配置
func init() {
	// ioc 初始化
	tools.DevelopmentSetup()

	// 从ioc里面取出需要别测试的对象
	Impl = app.GetInternalApp(resource.AppName).(resource.Service)
}
