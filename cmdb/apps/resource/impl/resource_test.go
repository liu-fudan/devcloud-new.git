package impl_test

import (
	"testing"
	"time"

	"gitee.com/liu-fudan/devcloud-new/cmdb/apps/resource"
)

func TestResourcePut(t *testing.T) {
	res := resource.NewResource()

	meta := resource.NewMeta()
	res.Meta = meta
	meta.Id = "ins-01"
	meta.CreateAt = time.Now().Unix()
	meta.Domain = "default"
	meta.Namespace = "default"
	meta.Env = "NULL"

	spec := resource.NewSpec()
	res.Spec = spec
	spec.Name = "测试1"
	spec.Cpu = 4
	spec.Memory = 9024

	status := resource.NewStatus()
	res.Status = status
	status.Phase = "Running"
	status.PrivateAddress = []string{"22.12.22.21"}

	res.AddTag(resource.NewKVTag(resource.TAG_PURPOSE_THIRDPARTY, "app", "app2333"))
	ins, err := Impl.Put(ctx, res)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestResourceSearch(t *testing.T) {
	req := resource.NewSearchRequest()
	// req.Tag["app"] = "app021"
	set, err := Impl.Search(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
