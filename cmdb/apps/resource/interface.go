package resource

import (
	context "context"

	"github.com/infraboard/mcube/http/request"
)

const (
	AppName = "resources"
)

type Service interface {
	// 资源保存,及支持更新也支持创建
	Put(context.Context, *Resource) (*Resource, error)
	RPCServer
}

func NewSearchRequest() *SearchRequest {
	return &SearchRequest{
		Tag:  map[string]string{},
		Page: request.NewDefaultPageRequest(),
	}
}
