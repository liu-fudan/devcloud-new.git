package cmd

import (
	"errors"
	"fmt"

	"gitee.com/liu-fudan/devcloud-new/cmdb/cmd/start"
	"gitee.com/liu-fudan/devcloud-new/cmdb/conf"
	"gitee.com/liu-fudan/devcloud-new/cmdb/version"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	"github.com/spf13/cobra"
)

var (
	confType string
	confFile string
	confETCD string
)

var vers bool

var RootCmd = &cobra.Command{
	Use:   "cmdb",
	Short: "用户中心",
	Long:  "用户中心",
	RunE: func(cmd *cobra.Command, args []string) error {
		if vers {
			fmt.Println(version.FullVersion())
			return nil
		}
		return cmd.Help()
	},
}

func init() {
	RootCmd.PersistentFlags().StringVarP(&confType, "config-type", "t", "file", "the service config type [file/env/etcd]")
	RootCmd.PersistentFlags().StringVarP(&confFile, "config-file", "f", "etc/config.toml", "the service config from file")
	RootCmd.PersistentFlags().StringVarP(&confETCD, "config-etcd", "e", "127.0.0.1:2379", "the service config from etcd")
	RootCmd.PersistentFlags().BoolVarP(&vers, "version", "v", false, "the cmdb version")
}

func Execute() {
	cobra.OnInitialize(initail)
	RootCmd.AddCommand(start.Cmd)
	err := RootCmd.Execute()
	cobra.CheckErr(err)
}

func initail() {
	// 初始化全局变量
	err := loadGlobalConfig(confType)
	cobra.CheckErr(err)

	// 初始化全局app
	err = app.InitAllApp()
	cobra.CheckErr(err)
}

func loadGlobalConfig(configType string) error {
	// 配置加载
	switch configType {
	case "file":
		err := conf.LoadConfigFromToml(confFile)
		if err != nil {
			return err
		}
	case "env":
		err := conf.LoadConfigFromEnv()
		if err != nil {
			return err
		}
	default:
		return errors.New("unknown config type")
	}

	return nil
}
