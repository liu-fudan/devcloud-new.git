package protocol

import (
	"net"

	"gitee.com/liu-fudan/devcloud-new/cmdb/conf"
	// "gitee.com/liu-fudan/devcloud-new/cmdb/protocol/auth"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	"gitee.com/liu-fudan/fu-dan-tools-package/logger"
	"github.com/infraboard/mcube/grpc/middleware/recovery"
	"google.golang.org/grpc"
)

func NewGRPCService() *GRPCService {
	rc := recovery.NewInterceptor(recovery.NewZapRecoveryHandler())
	grpcServer := grpc.NewServer(grpc.ChainUnaryInterceptor(
		rc.UnaryServerInterceptor(),
		// 加载gRPC服务端认证中间件
		// auth.NewGrpcAuther().AuthFunc,
	))

	return &GRPCService{
		svr: grpcServer,
		c:   conf.C(),
	}
}

type GRPCService struct {
	svr *grpc.Server
	c   *conf.Config
}

func (s *GRPCService) Start() {
	// 装载所有GRPC服务
	app.LoadGrpcApp(s.svr)

	lis, err := net.Listen("tcp", s.c.App.GRPC.Addr())
	if err != nil {
		logger.L().Debug().Msgf("listen grpc tcp conn error, %s", err)
		return
	}

	logger.L().Info().Msgf("GRPC 服务监听地址: %s", s.c.App.GRPC.Addr())
	if err := s.svr.Serve(lis); err != nil {
		if err == grpc.ErrServerStopped {
			logger.L().Info().Msg("service is stopped")
		}

		logger.L().Error().Msgf("start grpc service error, %s", err.Error())
		return
	}
}

func (s *GRPCService) Stop() error {
	s.svr.GracefulStop()
	return nil
}
