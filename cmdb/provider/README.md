# 资源提供商

hook机制将provider和resource解耦开，provider资源转换完后通过hook定义转换后的资源该怎么做。


## 何时出发定时同步

+ 程序里写一个cron, 请参考[Go语言中定时任务库Cron使用详解](https://blog.csdn.net/qq_43058685/article/details/123843956)
+ 基于Job调度平台来进行调度执行:
    + x-job 使用第三方的一些job管理工具
    + 自己写一个分布式的job调度工具(有难度)
    + k8s cronjob (最优解)