package provider

import (
	"context"
	"fmt"

	"gitee.com/liu-fudan/devcloud-new/cmdb/apps/resource"
	"gitee.com/liu-fudan/devcloud-new/cmdb/apps/resource/impl"
	"gitee.com/liu-fudan/devcloud-new/cmdb/conf"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
)

// 仅仅是打印下对象
var DefaultResourceHandleHook = func(ctx context.Context, res *resource.Resource) {
	fmt.Println(res)
}

var PutResourceHandleHook = func(ctx context.Context, res *resource.Resource) {
	err := conf.LoadConfigFromEnv()
	if err != nil {
		panic(err)
	}
	if err := app.InitAllApp(); err != nil {
		panic(err)
	}
	svc := impl.Svc
	svc.Put(ctx, res)
}

// Provider 完成资源转换后调用该Hook
type ResourceHandleHook func(ctx context.Context, res *resource.Resource)
