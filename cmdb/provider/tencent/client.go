package tencent

import (
	"gitee.com/liu-fudan/devcloud-new/cmdb/provider"
	"github.com/infraboard/mcube/flowcontrol/tokenbucket"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/profile"

	cvm "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/cvm/v20170312"
)

func NewTencentClient(secretId, secretKey string) *TencentClient {
	return &TencentClient{
		secretId:  secretId,
		secretKey: secretKey,
		hook:      provider.PutResourceHandleHook,
		pageSize:  50,
		tb:        tokenbucket.NewBucketWithRate(1, 5),
	}
}

// 腾讯云的客户端
type TencentClient struct {
	secretId  string
	secretKey string

	// 资源转换完成后的hook
	hook provider.ResourceHandleHook
	// 控制分页大小
	pageSize int64
	tb       *tokenbucket.Bucket
}

// 基于回调的写法(最直接, 损耗最小), js 就是这种用法的典型
func (c *TencentClient) SetResourceHandleHook(hook provider.ResourceHandleHook) {
	c.hook = hook
}

func (c *TencentClient) SetPageSize(pageSize int64) {
	c.pageSize = pageSize
}

// 实例化一个认证对象，入参需要传入腾讯云账户 SecretId 和 SecretKey，此处还需注意密钥对的保密
// 代码泄露可能会导致 SecretId 和 SecretKey 泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考，建议采用更安全的方式来使用密钥，请参见：https://cloud.tencent.com/document/product/1278/85305
// 密钥可前往官网控制台 https://console.cloud.tencent.com/cam/capi 进行获取
func (c *TencentClient) credential() *common.Credential {
	return common.NewCredential(
		c.secretId,
		c.secretKey,
	)
}

// 实例化一个client选项，可选的，没有特殊需求可以跳过
func (c *TencentClient) cvm() (*cvm.Client, error) {
	cpf := profile.NewClientProfile()
	cpf.HttpProfile.Endpoint = "cvm.tencentcloudapi.com"
	// 实例化要请求产品的client对象,clientProfile是可选的
	return cvm.NewClient(c.credential(), "ap-nanjing", cpf)
}
