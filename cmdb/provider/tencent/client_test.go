package tencent_test

import (
	"context"
	"os"

	"gitee.com/liu-fudan/devcloud-new/cmdb/provider/tencent"
)

var (
	ctx    = context.Background()
	client *tencent.TencentClient
)

func init() {
	client = tencent.NewTencentClient(
		os.Getenv("TX_SECRET_ID"),
		os.Getenv("TX_SECRET_KEY"),
		// "AKIDn2VDZtJoLrV9qXl4Nh2TtGfXND459CDB",
        // "Y4a7d9lNz146EOZm0DnQEMf8dAh8YuGc",
	)
}
