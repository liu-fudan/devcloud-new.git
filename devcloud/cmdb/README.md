# CMDB


## 需求

场景: 资源管理

运维: 应用维护 (excel维护资源和应用的关系)
  + 主机
  + IP
  + 证书
  + 中间件服务
  + LB
  + ...

CRM: 公司的业务与业务
  + 客户
  + 合同
  + 预算

## 核心功能

1. 资源录入？
2. 资源关系维护
3. 资源解索
   + 属性解索(模式): ip, 名字, ...
   + 关系解索: 获取一个应用的资源
4. 资源间关系: 图数据库  LB--->A(host)--->ENI--->IP

## 设计方案

我们到底有多少类型的资源需要管理? 衍生出了2个方案:
1. 基于资源类型进行模型
    + 定义模式: HOST, 定义模型属性: Field1, Field2
2. 基于业务 (V)
    + 通过MySQL定义Model, 创建表
  
1. 资源录入, 资源属性抽象:
   + meta: 通用属性, type, name, id, create_at, update_at, owner ..., extensions(扩展属性) 资源索引
   + spec: 每种资源独有的一些属性, 每种资源独立建表(host: os_name)
2. 关系维护与基于tag解索:
   + tag: tag表, 用于关联资源meta.id
3. 资源基于属性解索:
   + 通用属性: 直接通过meta表进行过滤,LIKE
   + 独有属性: 不推荐
4. 资源间关系: LB--->A(host)--->ENI--->IP
   meta:
    LB(id:lb01)
    A(id:host01)
    ENI(id:eni01)
    IP(id:ip01)
   relation_ship
    children parent
    ip01     eni01
    eni01    host01
    host01   lb01

## 业务设计

meta: 到底包含哪些属性(根据多个资源去抽象)
