package impl

import (
	"gitee.com/go-course/go9/projects/devcloud/cmdb/apps/resource"
	"gitee.com/go-course/go9/projects/devcloud/cmdb/conf"
	"github.com/infraboard/mcube/app"
	"google.golang.org/grpc"
	"gorm.io/gorm"
)

var (
	svc = &impl{}
)

// user service 的实例类？
// 之前如果进行实例类托管的, ioc 需要抽象到一个公共代码库管理
// mcube app
type impl struct {
	resource.UnimplementedRPCServer

	// db的依赖(MySQL)
	db *gorm.DB
}

// 实例类初始化
func (i *impl) Config() error {
	i.db = conf.C().MySQL.ORM().Debug()
	return nil
}

func (i *impl) Name() string {
	return resource.AppName
}

func (i *impl) Registry(server *grpc.Server) {
	resource.RegisterRPCServer(server, i)
}

func init() {
	// 应用公共库: mcube app
	// 注册grpc托管类
	app.RegistryGrpcApp(svc)
	// 注册内部服务托管类
	app.RegistryInternalApp(svc)
}
