package impl_test

import (
	"os"
	"testing"

	"gitee.com/go-course/go9/projects/devcloud/cmdb/apps/secret"
)

func TestSecretEncrypt(t *testing.T) {
	req := secret.NewCreateSecretRequest()
	req.ApiKey = os.Getenv("TX_SECRET_ID")
	req.ApiSecret = os.Getenv("TX_SECRET_KEY")
	req.Description = "老喻的腾讯云"

	ins, err := impl.CreateSecret(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQuerySecret(t *testing.T) {
	req := secret.NewQuerySecretRequest()
	ins, err := impl.QuerySecret(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestDescribeSecret(t *testing.T) {
	req := secret.NewDescribeSecretRequest("cgfchj13n7pjm31fks10")
	ins, err := impl.DescribeSecret(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	ins.DecryptAPISecret(secret.EncryptKey)
	t.Log(ins)
}

func TestSyncResource(t *testing.T) {
	req := secret.NewDescribeSecretRequest("cgfchj13n7pjm31fks10")
	err := impl.SyncResource(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
}
