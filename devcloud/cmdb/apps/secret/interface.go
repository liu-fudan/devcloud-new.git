package secret

import (
	"context"

	"github.com/infraboard/mcube/http/request"
)

const (
	AppName = "secrets"
)

type Service interface {
	// 通过该凭证的资源
	SyncResource(context.Context, *DescribeSecretRequest) error
	RPCServer
}

func NewQuerySecretRequest() *QuerySecretRequest {
	return &QuerySecretRequest{
		Page: request.NewDefaultPageRequest(),
	}
}

func NewDescribeSecretRequest(id string) *DescribeSecretRequest {
	return &DescribeSecretRequest{
		Id: id,
	}
}
