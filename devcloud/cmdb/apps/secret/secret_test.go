package secret_test

import (
	"testing"

	"gitee.com/go-course/go9/projects/devcloud/cmdb/apps/secret"
)

func TestSecretEncrypt(t *testing.T) {
	s := secret.New(secret.NewCreateSecretRequest())
	s.Spec.ApiKey = "TEST01"
	s.Spec.ApiSecret = "secret01"

	encryptKey := "test"

	// 加密
	err := s.EncryptAPISecret(encryptKey)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(s)

	// 解密
	err = s.DecryptAPISecret(encryptKey)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(s)
}
