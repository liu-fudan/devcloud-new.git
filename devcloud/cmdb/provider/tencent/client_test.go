package tencent_test

import (
	"context"
	"os"

	"gitee.com/go-course/go9/projects/devcloud/cmdb/provider/tencent"
)

var (
	ctx    = context.Background()
	client *tencent.TencentClient
)

func init() {
	client = tencent.NewTencentClient(
		os.Getenv("TX_SECRET_ID"),
		os.Getenv("TX_SECRET_KEY"),
	)
}
