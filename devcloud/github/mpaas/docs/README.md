# mpaas
轻量级的应用开发平台(CI/CD)， 发布完成后的管理工作(client-go 编写API 进行pod的管理)
## 需求
### 构建(CI)
+ 镜像构建: 
   + [如何在Docker 中使用 Docker](https://www.6hu.cc/archives/78414.html)
   + [Build Images In Kubernetes](https://github.com/GoogleContainerTools/kaniko)
   + [kubernetes【工具】kaniko【1】【介绍】-无特权构建镜像](https://blog.csdn.net/xixihahalelehehe/article/details/121659254)
+ 镜像部署: [kubectl 工具镜像](https://hub.docker.com/r/bitnami/kubectl)
+ 虚拟机部署: [在 Docker 容器中配置 Ansible](https://learn.microsoft.com/zh-cn/azure/developer/ansible/configure-in-docker-container?tabs=azure-cli)



1. 创建k8s托管集群(腾讯云/本地)
2. 托管k8s的kube_config
    + 把k8s的kubeconfig 文件复制到单元测试目录test下 kube_config.yml
    + 创建cluster: k8s_test 单元测试: TestCreateCluster
3. 创建一个docker_build的job  TestCreateBuildJob, TestUpdateBuildJob 修改job参数
    + 注意: 修改kube config 里面的cluster字段名为: k8s-test， 以便集群的名称是k8s-test， 方便后面的单元测试
4. 准备Job运行时需要的secret
    + git-ssh-key 用于拉取git仓库代码的secret名称, kubectl create secret generic git-ssh-key --from-file=id_rsa=${HOME}/.ssh/id_rsa
    + kaniko-secret  用于推送镜像的secret名称, 具体文档参考: https://github.com/GoogleContainerTools/kaniko#pushing-to-docker-hub: kubectl create secret generic kaniko-secret --from-file=apps/job/impl/test/config.json。 例如用阿里仓库通过registry用户名与密码以下命令获取
5. 测试Job的运行, 这里以Mcenter项目为例子进行Build Job的测试, 到 task模块下 进行Job的运行
   + 前缀条件: mcenter/mpaas/moperator, 启动顺序: mcenter, mpaas, moperator(提前在mcenter 通过单元测试创建)
   + mpaas,moperator 配置文件配置好, 特别是client凭证

如何调用k8s 的API 完成job的执行(典型的client-go的使用场景, 基于client-go的二开场景), task 模块下面的runner

+ 如何初始化client-go: provider/client.go
+ 基于client-go的job管理
+ 基于client-go的Deploment管理

operator 是如何更新job状态？watch k8s， 监听job的状态变化，和mpass进行对比。
### CI
配置两个secret，一个git-ssh-key拉git仓库代码(有的不填不让拉)，另一个是自己的镜像仓库的secret。init容器里安装git，把代码拉到公共volume，工具镜像执行构建，docker build 两种，第一种docker in pod，需要特权，危险。第二种是kaniko，需要配置secret，安全。
### 部署(CD)
通过 client-go 创建一个部署
### 基于gitlab的触发Pipeline的 CI/CD 
pipeline 的设计: 核心 负责job编排的定义

+ 创建PipelineLine
+ 运行Pipeline, 先不启动Operator, 手动更新Task状态, 测试触发流程

gitlab webhook事件的处理
### App(k8s二开)
查询部署
重启部署
查询日志