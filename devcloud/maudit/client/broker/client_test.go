package broker_test

import (
	"context"
	"testing"
	"time"

	"gitee.com/go-course/go9/projects/devcloud/maudit/apps/audit"
	"gitee.com/go-course/go9/projects/devcloud/maudit/client/broker"
	"github.com/rs/xid"
)

var (
	ctx = context.Background()
)

func TestSendAuditLog(t *testing.T) {
	client := broker.NewClient("localhost:9092", "topic-A")
	defer client.Close()

	err := client.SendAuditLog(ctx, audit.AuditLog{
		Id:        xid.New().String(),
		Username:  "test",
		Time:      time.Now().Unix(),
		ServiceId: "mpaas 01",
		Operate:   "QueryCluster",
		Request:   "xxx",
	})

	if err != nil {
		t.Fatal(err)
	}
}
