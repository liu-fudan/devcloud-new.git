package apps

import (
	_ "gitee.com/go-course/go9/projects/devcloud/mcenter/apps/endpoint/impl"
	_ "gitee.com/go-course/go9/projects/devcloud/mcenter/apps/permission/impl"
	_ "gitee.com/go-course/go9/projects/devcloud/mcenter/apps/policy/impl"
	_ "gitee.com/go-course/go9/projects/devcloud/mcenter/apps/role/impl"
	_ "gitee.com/go-course/go9/projects/devcloud/mcenter/apps/service/impl"
	_ "gitee.com/go-course/go9/projects/devcloud/mcenter/apps/token/impl"
	_ "gitee.com/go-course/go9/projects/devcloud/mcenter/apps/user/impl"
)
