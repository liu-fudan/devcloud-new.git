package impl_test

import (
	"context"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/role"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/test/tools"
	"github.com/infraboard/mcube/app"
)

var (
	// 声明需要测试的对象
	impl role.Service

	ctx = context.Background()
)

// 加载ioc, 需要加载测试用例的配置
func init() {
	// ioc 初始化
	tools.DevelopmentSetup()

	// 从ioc里面取出需要别测试的对象
	impl = app.GetInternalApp(role.AppName).(role.Service)
}
