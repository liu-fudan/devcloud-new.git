package impl_test

import (
	"testing"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/role"
)

func TestCreateRole(t *testing.T) {
	req := role.NewCreateRoleRequest()
	req.Name = "admin"
	req.AddFeature(&role.Feature{
		ServiceId:  "cfsrgnh3n7pi7u2is87g",
		HttpMethod: "GET",
		HttPath:    "/mpaas/api/v1/clusters",
	})
	ins, err := impl.CreateRole(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQueryRole(t *testing.T) {
	req := role.NewQueryRoleRequest()
	req.AddRoleId("cg5uklh3n7pjo26e8gtg")
	set, err := impl.QueryRole(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
