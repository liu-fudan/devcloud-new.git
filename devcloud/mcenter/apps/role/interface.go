package role

import (
	context "context"

	"github.com/infraboard/mcube/http/request"
)

const (
	AppName = "roles"
)

type Service interface {
	// 创建角色
	CreateRole(context.Context, *CreateRoleRequest) (*Role, error)
	RPCServer
}

func NewQueryRoleRequest() *QueryRoleRequest {
	return &QueryRoleRequest{
		Page: request.NewDefaultPageRequest(),
	}
}

func (req *QueryRoleRequest) AddRoleId(ids ...string) {
	for i := range ids {
		req.RoleIds = append(req.RoleIds, ids[i])
	}
}
