package impl_test

import (
	"testing"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/service"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/test/tools"
)

func TestCreateService(t *testing.T) {
	req := &service.CreateServiceRequest{
		Domain:    "default",
		Namespace: "default",
		Name:      "mpaas",
	}

	ins, err := impl.CreateService(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}

func TestDescribeServiceByCrendentail(t *testing.T) {
	req := &service.DescribeServiceRequest{
		DescribeBy:    service.DESCRIBE_BY_SERVICE_CREDENTAIL_ID,
		DescribeVaule: "cfsqmsh3n7pkct5giljg",
	}

	ins, err := impl.DescribeService(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}

func TestDescribeServiceById(t *testing.T) {
	req := &service.DescribeServiceRequest{
		DescribeBy:    service.DESCRIBE_BY_SERVICE_ID,
		DescribeVaule: "cfsrgnh3n7pi7u2is87g",
	}

	ins, err := impl.DescribeService(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}
