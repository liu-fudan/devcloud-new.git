package impl

import (
	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/token"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/conf"
	"github.com/infraboard/mcube/app"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/token/provider"
	// 需要初始化Provider
	_ "gitee.com/go-course/go9/projects/devcloud/mcenter/apps/token/provider/all"
)

var (
	svc = &impl{}
)

// user service 的实例类？
// 之前如果进行实例类托管的, ioc 需要抽象到一个公共代码库管理
// mcube app
type impl struct {
	token.UnimplementedRPCServer

	// 依赖数据库, 在配置对象里面
	col *mongo.Collection
}

// 实例类初始化
func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection("tokens")

	return provider.Init()
}

func (i *impl) Name() string {
	return token.AppName
}

func (i *impl) Registry(server *grpc.Server) {
	token.RegisterRPCServer(server, i)
}

func init() {
	// 应用公共库: mcube app
	// 注册grpc托管类
	app.RegistryGrpcApp(svc)
	// 注册内部服务托管类
	app.RegistryInternalApp(svc)
}
