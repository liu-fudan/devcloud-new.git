package impl_test

import (
	"testing"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/token"
	"github.com/infraboard/mcube/exception"
)

func TestIssueToken(t *testing.T) {
	req := &token.IssueTokenRequest{
		Username: "admin",
		Password: "123456",
	}
	tk, err := impl.IssueToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func TestValidateToken(t *testing.T) {
	req := token.NewValidateTokenRequest("cfsmde93n7pk5d7qep20")
	tk, err := impl.ValidateToken(ctx, req)
	if err != nil {
		t.Log(err.(exception.APIException).ToJson())
		t.Fatal(err)
	}
	t.Log(tk)
}
