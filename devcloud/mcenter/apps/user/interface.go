package user

import (
	context "context"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/domain"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/common/validator"
	"github.com/infraboard/mcube/http/request"
)

const (
	AppName = "users"
)

// 内部接口, 模块内调用
type Service interface {
	// 用户的创建
	CreateUser(context.Context, *CreateUserRequest) (*User, error)
	// 用户更新
	UpdateUser(context.Context, *UpdateUserRequest) (*User, error)
	// 用户删除
	DeleteUser(context.Context, *DeleteUserRequest) (*User, error)
	RPCServer
}

func NewCreateUserRequest() *CreateUserRequest {
	return &CreateUserRequest{}
}

func (req *CreateUserRequest) Validate() error {
	if req.Domain == "" {
		req.Domain = domain.DEFAULT_DOMAIN
	}
	return validator.Validate(req)
}

func NewQueryUserRequest() *QueryUserRequest {
	return &QueryUserRequest{
		Page: request.NewDefaultPageRequest(),
	}
}

func NewDescribeUserRequestByUsername(username string) *DescribeUserRequest {
	return &DescribeUserRequest{
		DescribeBy:    DESCRIBE_BY_USERNAME,
		DescribeVaule: username,
	}
}
