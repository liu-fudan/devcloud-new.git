package user

import (
	"encoding/json"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/common/meta"
	"golang.org/x/crypto/bcrypt"
)

func NewUserSet() *UserSet {
	return &UserSet{
		Items: []*User{},
	}
}

func (s *UserSet) Add(item *User) {
	s.Items = append(s.Items, item)
}

func NewDefaultUser() *User {
	return &User{
		Meta: meta.NewMeta(),
		Spec: NewCreateUserRequest(),
	}
}

func (i *User) Desense() {
	i.Spec.Password = "***"
}

func (i *User) CheckPassword(pass string) error {
	return bcrypt.CompareHashAndPassword([]byte(i.Spec.Password), []byte(pass))
}

func New(req *CreateUserRequest) (*User, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}

	if err := req.HashPassword(); err != nil {
		return nil, err
	}

	return &User{
		Meta: meta.NewMeta(),
		Spec: req,
	}, nil
}

func (req *CreateUserRequest) HashPassword() error {
	// 专门用户Password hash的方法
	hp, err := bcrypt.GenerateFromPassword([]byte(req.Password), 10)
	if err != nil {
		return err
	}
	req.Password = string(hp)
	return nil
}

// 自定义对象序号化方法
func (u *User) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		*meta.Meta
		*CreateUserRequest
	}{u.Meta, u.Spec})
}
