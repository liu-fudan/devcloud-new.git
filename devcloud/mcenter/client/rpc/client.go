package rpc

import (
	"context"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/endpoint"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/permission"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/token"
)

// NewClient todo
func NewClient(conf *Config) (*ClientSet, error) {
	ctx, cancel := context.WithTimeout(context.Background(), conf.Timeout())
	defer cancel()

	conn, err := grpc.DialContext(
		ctx,
		// mcenter Grpc 服务地址
		conf.Address,
		// 不使用TLS
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		// 传递认证信息
		grpc.WithPerRPCCredentials(conf),
	)

	if err != nil {
		return nil, err
	}

	return &ClientSet{
		conf: conf,
		conn: conn,
	}, nil
}

type ClientSet struct {
	conf *Config
	conn *grpc.ClientConn
}

// token rpc 客户端
func (c *ClientSet) Token() token.RPCClient {
	return token.NewRPCClient(c.conn)
}

// 功能管理的 rpc 客户端
func (c *ClientSet) Endpoint() endpoint.RPCClient {
	return endpoint.NewRPCClient(c.conn)
}

// permission rpc 客户端
func (c *ClientSet) Permission() permission.RPCClient {
	return permission.NewRPCClient(c.conn)
}
