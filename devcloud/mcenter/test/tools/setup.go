package tools

import (
	// "github.com/infraboard/mcenter/conf"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/conf"
	"github.com/infraboard/mcube/app"

	// 注册所有服务
	_ "gitee.com/go-course/go9/projects/devcloud/mcenter/apps"
)

func DevelopmentSetup() {
	// 初始化配置, 提前配置好/etc/unit_test.env
	err := conf.LoadConfigFromEnv()
	if err != nil {
		panic(err)
	}

	// 初始化全局app
	if err := app.InitAllApp(); err != nil {
		panic(err)
	}
}
