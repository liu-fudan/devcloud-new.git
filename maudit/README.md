# 审计中心
记录用户行为: who when 在哪个服务进行了什么操作

其他服务将审计日志发给卡夫卡，再由maudit处理。
rpc需要等待返回，处理日志并没有返回的需求，不需要阻塞等待。
可以用goroutine 异步处理或者直接发送到mq。
这里选择用mq。信息的格式如何统一。
由maudit提供统一client 给其他服务接入(发送功能)。

### kafka启动
master虚拟机 docker-compose up -d
联通主机与虚机container，先docker inspect查看容器IPAddress。查看虚机ens33的inet
管理员打开cmd ROUTE add 容器子网 mask 255.255.0.0 ens33.inet
ROUTE add 172.18.0.0 mask 255.255.0.0 192.168.230.151

docker-compose要添加   KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://192.168.230.151:9092
把kafka的地址端口注册给zookeeper，若远程访问要改成外网IP,这里虚机用的nat，直接用虚机的ens33.inet

docker exec -it kafka /bin/bash 
 cd /opt/kafka/bin
 ./kafka-topics.sh --zookeeper 172.18.0.2:2181 --list // zookeeper地址是它的容器地址


 go get gitee.com/liu-fudan/devcloud-new/mcenter@5433953 