package api

import (
	"gitee.com/liu-fudan/devcloud-new/maudit/apps/audit"
	"gitee.com/liu-fudan/devcloud-new/maudit/common/errors"
	"github.com/emicklei/go-restful/v3"
)

func (h *handler) QueryAuditLog(r *restful.Request, w *restful.Response) {
	in := audit.NewQueryRequest()
	err := r.ReadEntity(in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}
	user, err := h.service.QueryAuditLog(r.Request.Context(), in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}

	w.WriteEntity(user)
}
