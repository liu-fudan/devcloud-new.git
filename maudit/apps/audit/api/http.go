package api

import (
	"gitee.com/liu-fudan/devcloud-new/maudit/apps/audit"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
)

type handler struct {
	service audit.Service
}

var (
	h = &handler{}
)

func (h *handler) Config() error {
	h.service = app.GetInternalApp(audit.AppName).(audit.Service)
	return nil
}

func (h *handler) Name() string {
	return audit.AppName
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{"审计"}
	ws.Route(ws.POST("/").To(h.QueryAuditLog).
		Doc("查询审计日志").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Metadata("service", "corjurimpgso852orq3g").
		Metadata("auth", true).
		Metadata("perm", true).
		Reads(audit.QueryAuditLogRequest{}).
		Writes(audit.AuditLogSet{}).
		Returns(200, "OK", audit.AuditLogSet{}))
}

func init() {
	app.RegistryRESTfulApp(h)
}
