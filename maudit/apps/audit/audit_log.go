package audit

import (
	"encoding/json"
	"fmt"

	"gitee.com/liu-fudan/fu-dan-tools-package/http/request"
)

type AuditLogSet struct {
	Total int64       `json:"total"`
	Items []*AuditLog `json:"items"`
}

func LoadAuditLogFromJosn(data []byte) (*AuditLog, error) {
	ins := NewAuditLog()
	err := json.Unmarshal(data, ins)
	if err != nil {
		return nil, err
	}
	return ins, nil
}

func NewAuditLog() *AuditLog {
	return &AuditLog{}
}

func NewAuditLogSet() *AuditLogSet {
	return &AuditLogSet{
		Items: []*AuditLog{},
	}
}

// 审计日志
type AuditLog struct {
	Id       string `bson:"_id" json:"id"`
	Username string `bson:"username" json:"username"`
	Time     int64  `bson:"time" json:"time"`
	// 访问那个功能
	ServiceId string `bson:"service_id" json:"service_id"`
	// 操作
	Operate string `bson:"operate" json:"operate"`
	// 具体的请求内容
	Request string `bson:"request" json:"request"`
}

func (s *AuditLog) String() string {
	b, err := json.Marshal(s)
	if err != nil {
		fmt.Println(err)
	}
	return string(b)
}

func (s *AuditLogSet) Add(item *AuditLog) {
	s.Items = append(s.Items, item)
}

func (s *AuditLog) ToJsonByte() []byte {
	b, err := json.Marshal(s)
	if err != nil {
		fmt.Println(err)
	}
	return b
}

func NewQueryRequest() *QueryAuditLogRequest {
	return &QueryAuditLogRequest{
		Page: request.NewDefaultPageRequest(),
	}
}
