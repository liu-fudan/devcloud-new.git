package impl

import (
	"context"

	"gitee.com/liu-fudan/fu-dan-tools-package/logger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitee.com/liu-fudan/devcloud-new/maudit/apps/audit"
)

// 日志保存
func (i *impl) SaveAuditLog(ctx context.Context, in *audit.AuditLog) (
	*audit.AuditLog, error) {
	logger.L().Debug().Msgf("save log: %s", in)
	_, err := i.col.InsertOne(ctx, in)
	if err != nil {
		return nil, err
	}

	return in, nil
}

// 日志查询
func (i *impl) QueryAuditLog(ctx context.Context, in *audit.QueryAuditLogRequest) (
	*audit.AuditLogSet, error) {
	set := audit.NewAuditLogSet()

	// 构造查询条件
	filter := bson.M{}
	opts := &options.FindOptions{}
	opts.SetLimit(int64(in.Page.PageSize))
	opts.SetSkip(in.Page.ComputeOffset())

	cursor, err := i.col.Find(ctx, filter, opts)
	if err != nil {
		return nil, err
	}

	// 执行查询SQL
	for cursor.Next(ctx) {
		ins := audit.NewAuditLog()
		if err := cursor.Decode(ins); err != nil {
			return nil, err
		}
		set.Add(ins)
	}

	// 统计总数
	set.Total, err = i.col.CountDocuments(ctx, filter)
	if err != nil {
		return nil, err
	}
	return set, nil
}
