package impl_test

import (
	"testing"

	"gitee.com/liu-fudan/devcloud-new/maudit/apps/audit"
)

func TestQueryAuditLog(t *testing.T) {
	req := audit.NewQueryAuditLogRequest()
	tk, err := impl.QueryAuditLog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log((tk))

}

 
func TestSaveAuditLog(t *testing.T) {
	req := audit.NewAuditLog()
	req.Id = "11111"
	tk, err := impl.SaveAuditLog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log((tk))

}