package impl

import (
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	"go.mongodb.org/mongo-driver/mongo"

	"gitee.com/liu-fudan/devcloud-new/maudit/apps/audit"
	"gitee.com/liu-fudan/devcloud-new/maudit/conf"
)

var (
	svc = &impl{}
)

type impl struct {
	col *mongo.Collection
}

// 实例类初始化
func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection(audit.AppName)

	return nil
}

func (i *impl) Name() string {
	return audit.AppName
}

func init() {
	app.RegistryInternalApp(svc)
}
