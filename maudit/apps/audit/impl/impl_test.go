package impl_test

import (
	"context"

	"gitee.com/liu-fudan/devcloud-new/maudit/apps/audit"
	"gitee.com/liu-fudan/devcloud-new/maudit/test/tools"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
)

var (
	impl audit.Service
	ctx  = context.Background()
)

func init() {
	tools.DevelopmentSetup()

	impl = app.GetInternalApp(audit.AppName).(audit.Service)
}
