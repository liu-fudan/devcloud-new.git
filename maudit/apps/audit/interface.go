package audit

import "context"
import request "gitee.com/liu-fudan/fu-dan-tools-package/http/request"

const (
	AppName = "audit"
)

// 审计日志接口
type Service interface {
	// 日志保存
	SaveAuditLog(context.Context, *AuditLog) (*AuditLog, error)
	// 日志查询
	QueryAuditLog(context.Context, *QueryAuditLogRequest) (*AuditLogSet, error)
}

type QueryAuditLogRequest struct {
	// 分页参数
	Page *request.PageRequest `protobuf:"bytes,1,opt,name=page,proto3" json:"page,omitempty"`
}

func NewQueryAuditLogRequest() *QueryAuditLogRequest {
	return &QueryAuditLogRequest{
		Page: request.NewDefaultPageRequest(),
	}
}