package audit

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"strings"
	"time"

	"gitee.com/liu-fudan/devcloud-new/maudit/apps/audit"
	"gitee.com/liu-fudan/devcloud-new/maudit/client/broker"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/token"
	"gitee.com/liu-fudan/devcloud-new/mcenter/client/rpc"
	"gitee.com/liu-fudan/fu-dan-tools-package/logger"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/http/response"
	"github.com/rs/xid"
)

func NewHttpAuther() *HttpAuther {
	return &HttpAuther{
		client: *broker.NewClient("172.18.0.3:9092", "topic-A"),
	}
}

type HttpAuther struct {
	client broker.Client
}

var rpcClient *rpc.ClientSet
var err error

func init() {
	config := rpc.NewDefaultConfig()
	config.ClientID = "corjurimpgso852orq40"
	config.ClientSecret = "corjurimpgso852orq4g"
	rpcClient, err = rpc.NewClient(config)
	if err != nil {
		panic(1)
	}
}

func (a *HttpAuther) AuthFunc(req *restful.Request, resp *restful.Response, next *restful.FilterChain) {
	// 权限判断：用户当前访问了哪个接口
	accessRoute := req.SelectedRoute()
	serviceMeta := accessRoute.Metadata()["service"]
	if serviceMeta == nil { // 不是服务调用的
	} else {
		serviceId := serviceMeta.(string)

		isAuth := accessRoute.Metadata()["auth"]
		username := ""
		if isAuth != nil && isAuth.(bool) {
			authHeader := req.HeaderParameter(token.TOKEN_HEADER_KEY)
			tkl := strings.Split(authHeader, " ") // Authorization: breaer xxxx
			if len(tkl) != 2 {
				response.Failed(resp, errors.New("令牌不合法, 格式： Authorization: breaer xxxx"))
				return
			}

			tk := tkl[1]
			logger.L().Debug().Msgf("get token: %s", tk)

			// 检查Token的合法性, 需要通过RPC进行检查
			tkObj, err := rpcClient.Token().ValidateToken(
				req.Request.Context(),
				token.NewValidateTokenRequest(tk),
			)
			if err != nil {
				retErr := errors.New(fmt.Sprintf("令牌校验不合法, %s", err))
				resp.Write([]byte(retErr.Error()))
				return
			}
			username = tkObj.Username
		}

		requestBody, _ := io.ReadAll(req.Request.Body)
		req.Request.Body = io.NopCloser(bytes.NewBuffer(requestBody[:]))

		log := audit.AuditLog{
			Id:        xid.New().String(),
			Username:  username,
			Time:      time.Now().Unix(),
			ServiceId: serviceId,
			Operate:   accessRoute.Method() + " " + accessRoute.Path(),
			Request:   string(requestBody),
		}
		// fmt.Println(log)
		// req.Request.Body = io.NopCloser(bytes.NewReader(requestBody))

		a.client.SendAuditLog(req.Request.Context(), log)
	}

	// 交给下个处理
	next.ProcessFilter(req, resp)
}
