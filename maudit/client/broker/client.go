package broker

import (
	"context"
	"strings"

	"github.com/segmentio/kafka-go"

	"gitee.com/liu-fudan/devcloud-new/maudit/apps/audit"
)

const (
	DEFAULT_TOPIC = "audit_log"
)

func NewDefaultClient(brokers string) *Client {
	return NewClient(brokers, DEFAULT_TOPIC)
}

func NewClient(brokers, topic string) *Client {
	adress := strings.Split(brokers, ",")
	w := &kafka.Writer{
		Addr:                   kafka.TCP(adress...),
		Topic:                  topic,
		Balancer:               &kafka.LeastBytes{},
		AllowAutoTopicCreation: false,
	}
	return &Client{
		brokerAddress: adress,
		writer:        w,
	}
}

type Client struct {
	brokerAddress []string
	writer        *kafka.Writer
}

func (c *Client) Close() error {
	return c.writer.Close()
}

// 发送审计日志
func (c *Client) SendAuditLog(ctx context.Context, al audit.AuditLog) error {
	m := kafka.Message{
		Key:   []byte(al.ServiceId),
		Value: al.ToJsonByte(),
	}

	return c.writer.WriteMessages(ctx, m)
}
