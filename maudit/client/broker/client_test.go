package broker_test

import (
	"context"
	"testing"
	"time"

	"github.com/rs/xid"

	"gitee.com/liu-fudan/devcloud-new/maudit/apps/audit"
	"gitee.com/liu-fudan/devcloud-new/maudit/client/broker"
)

var (
	ctx = context.Background()
)

func TestSendAuditLog(t *testing.T) {
	client := broker.NewClient("172.18.0.3:9092", "topic-A")
	defer client.Close()

	err := client.SendAuditLog(ctx, audit.AuditLog{
		Id:        xid.New().String(),
		Username:  "test",
		Time:      time.Now().Unix(),
		ServiceId: "mpaas 012",
		Operate:   "QueryCluster",
		Request:   "xxx",
	})

	if err != nil {
		t.Fatal(err)
	}
}
