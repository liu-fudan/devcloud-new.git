package errors

type Exception struct {
	Code    int
	Message string
}
