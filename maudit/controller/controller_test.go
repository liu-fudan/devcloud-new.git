package controller_test

import (
	"context"
	"testing"
	"time"

	"gitee.com/liu-fudan/devcloud-new/maudit/controller"
	"gitee.com/liu-fudan/devcloud-new/maudit/test/tools"
)

var (
	ctx = context.Background()
)

func TestAuduitLogSaveConroller(t *testing.T) {
	c := controller.NewAuduitLogSaveConroller(
		[]string{"172.18.0.3:9092"},
		"consumer-group-test",
		"topic-A",
	)

	go c.Run(ctx)

	time.Sleep(15 * time.Second)
	err := c.Stop()
	if err != nil {
		t.Fatal(err)
	}
}

func init() {
	tools.DevelopmentSetup()
}
