package tools

import (
	// 注册所有服务

	"gitee.com/liu-fudan/fu-dan-tools-package/app"

	_ "gitee.com/liu-fudan/devcloud-new/maudit/apps"
	"gitee.com/liu-fudan/devcloud-new/maudit/conf"
)

func DevelopmentSetup() {
	// 初始化配置, 提前配置好/etc/unit_test.env
	err := conf.LoadConfigFromEnv()
	if err != nil {
		panic(err)
	}

	// 初始化全局app
	if err := app.InitAllApp(); err != nil {
		panic(err)
	}
}
