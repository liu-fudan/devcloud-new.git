# 用户中心 
+ 认证
  + 登录: 换取身份令牌(Token)
    + UserPassword: 基于用户名密码来获取身份令牌

+ 鉴权


### ioc


### 为什么用mongo
1. 建库建表比较方便。更改字段也不需要动表

设计发布中心时有kubernetes的deployment的概念，用关系型数据库直接存或者拆分(deployment已经原子化了)都不太好

像blob和text这样的字段，名义上是为存储很大的数据而设计的类型。但这正因为如此，这跟关系数据库使用table的设计理念是冲突的。table中的每一列数据都是定长的，比如varchar(32)。但blob和text的上限太长了，MySQL不可能将它们存储在table中，因而会使用专门的外部存储区域进行存储，数据行内存储指针。这样做的其中一个结果是会导到多一次磁盘IO，性能开销比较严重。

# 更新
go get gitee.com/liu-fudan/devcloud-new/maudit@ 