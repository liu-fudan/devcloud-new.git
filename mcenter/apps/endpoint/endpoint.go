package endpoint

import (
	"fmt"
	"hash/fnv"

	"gitee.com/liu-fudan/devcloud-new/mcenter/common/meta"
)

func NewEndpiontSet() *EndpointSet {
	return &EndpointSet{
		Items: []*Endpoint{},
	}
}

func (s *EndpointSet) Add(item *Endpoint) {
	s.Items = append(s.Items, item)
}

func NewRegistryRequest() *RegistryRequest {
	return &RegistryRequest{
		Items: []*CreateEndpointRequest{},
	}
}

func (r *CreateEndpointRequest) UnitKey() string {
	return fmt.Sprintf("%s.%s.%s", r.ServiceId, r.Method, r.Path)
}

func (s *RegistryRequest) Add(item *CreateEndpointRequest) {
	s.Items = append(s.Items, item)
}

func New(spec *CreateEndpointRequest) *Endpoint {
	ep := &Endpoint{
		Meta: meta.NewMeta(),
		Spec: spec,
	}
	h := fnv.New32a()
	_, err := h.Write([]byte(ep.Spec.UnitKey()))
	if err != nil {
		panic(err)
	}
	ep.Meta.Id = fmt.Sprintf("%d", h.Sum32())
	return ep
}
func NewEndpoint(serviceId, method, path, operation string) *Endpoint {
	return &Endpoint{
		Spec: NewCreateEndpointRequest(serviceId, method, path, operation),
	}
}

func NewCreateEndpointRequest(serviceId, method, path, operation string) *CreateEndpointRequest {
	return &CreateEndpointRequest{
		ServiceId: serviceId, Method: method, Path: path, Operation: operation,
	}
}

func NewDefaultCreateEndpointRequest() *CreateEndpointRequest {
	return &CreateEndpointRequest{}
}

func NewDefaultEndpoint() *Endpoint {
	return &Endpoint{
		Meta: meta.NewMeta(),
		Spec: NewDefaultCreateEndpointRequest(),
	}
}

func (e *Endpoint) SetAuth(isAuth bool) {
	e.Spec.Auth = isAuth
}

func (e *Endpoint) IsAuth() bool {
	return e.Spec.Auth
}

func (e *Endpoint) SetPerm(isPerm bool) {
	e.Spec.Perm = isPerm
}

func (e *Endpoint) IsPerm() bool {
	return e.Spec.Perm
}
