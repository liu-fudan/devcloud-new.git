package impl_test

import (
	"testing"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/endpoint"
)

func TestRegistryEndpoint(t *testing.T) {
	req := &endpoint.RegistryRequest{
		Items: []*endpoint.CreateEndpointRequest{},
	}
	req.Items = append(req.Items, &endpoint.CreateEndpointRequest{
		ServiceId: "service01",
		Method:    "POST",
		Path:      "xxx",
		Operation: "xxx",
	})
	set, err := impl.RegistryEndpoint(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

func TestQueryEnpoint(t *testing.T) {
	req := endpoint.NewQueryEnpointRequest()
	set, err := impl.QueryEnpoint(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
