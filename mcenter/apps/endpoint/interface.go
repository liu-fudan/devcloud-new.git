package endpoint

import "gitee.com/liu-fudan/fu-dan-tools-package/http/request"

const (
	AppName = "endpoints"
)

type Service interface {
	RPCServer
}

func NewQueryEnpointRequest() *QueryEnpointRequest {
	return &QueryEnpointRequest{
		Page: request.NewDefaultPageRequest(),
	}
}
