package impl

import (
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/permission"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/policy"
	"gitee.com/liu-fudan/devcloud-new/mcenter/conf"
)

var (
	svc = &impl{}
)

type impl struct {
	permission.UnimplementedRPCServer
	col    *mongo.Collection
	policy policy.Service
}

func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection(permission.AppName)

	i.policy = app.GetInternalApp(policy.AppName).(policy.Service)
	return nil
}

func (i *impl) Name() string {
	return permission.AppName
}

func (i *impl) Registry(server *grpc.Server) {
	permission.RegisterRPCServer(server, i)
}

func init() {
	app.RegistryGrpcApp(svc)
	app.RegistryInternalApp(svc)
}
