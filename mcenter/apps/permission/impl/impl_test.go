package impl_test

import (
	"context"

	"gitee.com/liu-fudan/fu-dan-tools-package/app"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/permission"
	"gitee.com/liu-fudan/devcloud-new/mcenter/test/tools"
)

var (
	impl permission.Service

	ctx = context.Background()
)

func init() {
	tools.DevelopmentSetup()

	impl = app.GetInternalApp(permission.AppName).(permission.Service)
}
