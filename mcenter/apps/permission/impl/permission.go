package impl

import (
	"context"

	"gitee.com/liu-fudan/fu-dan-tools-package/logger"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/permission"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/policy"
)

// 权限校验
func (i *impl) CheckPermission(ctx context.Context, in *permission.CheckPermissionRequest) (
	*permission.CheckPermissionResponse, error) {
	logger.L().Debug().Msgf("check permission request: %s", in)

	// 1. 查询出用户的策略
	policyQuery := policy.NewQueryPolicyRequest()
	policyQuery.UserId = in.UserId
	policyQuery.Namespace = in.Namespace
	policyQuery.WithRole = true
	ps, err := i.policy.QueryPolicy(ctx, policyQuery)
	if err != nil {
		logger.L().Error().Msgf("queryPolicy failed")
		return nil, err
	}
	logger.L().Debug().Msgf("policy: %v", ps)

	checkResp := permission.NewCheckPermissionResponse()
	// 2. 根据该用户的角色, 判断权限
	roles := ps.GetRoles()
	logger.L().Debug().Msgf("roles: %v", roles)
	for i := range roles {
		r := roles[i]
		if r.HasFeatrue(in.ServiceId, in.HttpMethod, in.HttpPath) {
			checkResp.HasPermisson = true
			checkResp.Role = r
			return checkResp, nil
		}
	}

	return checkResp, nil
}
