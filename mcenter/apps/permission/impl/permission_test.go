package impl_test

import (
	"testing"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/permission"
)

// user_id:"cfotd7p3n7pjgb2t4lig"
// namespace:"default"
// service_id:"cfsrgnh3n7pi7u2is87g"
// http_method:"GET"
// http_path:"/mpaas/api/v1/clusters"
func TestCheckPermission(t *testing.T) {
	req := permission.NewCheckPermissionRequest()
	req.UserId = "colgph2mpgsn470eiing"
	req.Namespace = "default"
	req.ServiceId = "service01"
	req.HttpMethod = "POST"
	req.HttpPath = "cmdb/api/v1/resource"
	ins, err := impl.CheckPermission(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	if !ins.HasPermisson {
		t.Fatal(err)
	}
	t.Log(ins)
}
