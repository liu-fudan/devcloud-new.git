package api

import (
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/policy"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
)

type handler struct {
	service policy.Service
}

var (
	h = &handler{}
)

func (h *handler) Config() error {
	h.service = app.GetInternalApp(policy.AppName).(policy.Service)
	return nil
}

func (h *handler) Name() string {
	return policy.AppName
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{"策略"}
	ws.Route(ws.POST("/").To(h.CreatePolicy).
		Doc("创建策略").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(policy.CreatePolicyRequest{}).
		Metadata("service", "colh0e2mpgsgv37i2kl0").
		Writes(policy.Policy{}).
		Returns(200, "OK", policy.Policy{}))
	ws.Route(ws.POST("/list").To(h.QueryPolicy).
		Doc("查询策略列表").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Metadata("service", "colh0e2mpgsgv37i2kl0").
		Reads(policy.QueryPolicyRequest{}).
		Metadata("auth", true).
		Writes(policy.PolicySet{}).
		Returns(200, "OK", policy.PolicySet{}))
}

func init() {
	app.RegistryRESTfulApp(h)
}
