package api

import (
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/policy"
	"gitee.com/liu-fudan/devcloud-new/mcenter/common/errors"
	"github.com/emicklei/go-restful/v3"
)

func (h *handler) CreatePolicy(r *restful.Request, w *restful.Response) {
	in := policy.NewCreatePolicyRequest()
	err := r.ReadEntity(in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}
	user, err := h.service.CreatePolicy(r.Request.Context(), in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}

	w.WriteEntity(user)
}

func (h *handler) QueryPolicy(r *restful.Request, w *restful.Response) {
	in := policy.NewQueryPolicyRequest()
	err := r.ReadEntity(in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}
	user, err := h.service.QueryPolicy(r.Request.Context(), in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}

	w.WriteEntity(user)
}
