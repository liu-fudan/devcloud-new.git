package impl

import (
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/policy"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/role"
	"gitee.com/liu-fudan/devcloud-new/mcenter/conf"
)

var (
	svc = &impl{}
)

type impl struct {
	policy.UnimplementedRPCServer
	col  *mongo.Collection
	role role.Service
}

// 实例类初始化
func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection(policy.AppName)

	i.role = app.GetInternalApp(role.AppName).(role.Service)
	return nil
}

func (i *impl) Name() string {
	return policy.AppName
}

func (i *impl) Registry(server *grpc.Server) {
	policy.RegisterRPCServer(server, i)
}

func init() {
	app.RegistryGrpcApp(svc)
	app.RegistryInternalApp(svc)
}
