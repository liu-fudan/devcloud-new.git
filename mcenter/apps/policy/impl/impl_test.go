package impl_test

import (
	"context"

	"gitee.com/liu-fudan/fu-dan-tools-package/app"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/policy"
	"gitee.com/liu-fudan/devcloud-new/mcenter/test/tools"
)

var (
	impl policy.Service

	ctx = context.Background()
)

func init() {
	tools.DevelopmentSetup()
	impl = app.GetInternalApp(policy.AppName).(policy.Service)
}
