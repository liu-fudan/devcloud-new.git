package impl_test

import (
	"testing"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/policy"
)

func TestCreatePolicy(t *testing.T) {
	req := policy.NewCreatePolicyRequest()
	req.Namespace = "default"
	req.RoleId = "colgqv2mpgsm1d6qacvg"
	req.UserId = "colgph2mpgsn470eiing"
	ins, err := impl.CreatePolicy(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQueryPolicy(t *testing.T) {
	req := policy.NewQueryPolicyRequest()
	req.UserId = "cofmtj2mpgsivj6151qg"
	req.Namespace = "default"
	req.WithRole = true
	ins, err := impl.QueryPolicy(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}
