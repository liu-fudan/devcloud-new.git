package policy

import (
	"gitee.com/liu-fudan/fu-dan-tools-package/http/request"

	meta "gitee.com/liu-fudan/devcloud-new/mcenter/common/meta"
)

const AppName = "policy"

type Service interface {
	RPCServer
}

func NewCreatePolicyRequest() *CreatePolicyRequest {
	return &CreatePolicyRequest{}
}

func New(req *CreatePolicyRequest) *Policy {
	return &Policy{
		Meta: meta.NewMeta(),
		Spec: req,
	}
}

func NewQueryPolicyRequest() *QueryPolicyRequest {
	return &QueryPolicyRequest{
		Page: request.NewDefaultPageRequest(),
	}
}
