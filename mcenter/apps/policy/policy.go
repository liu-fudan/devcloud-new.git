package policy

import (
	role "gitee.com/liu-fudan/devcloud-new/mcenter/apps/role"
	meta "gitee.com/liu-fudan/devcloud-new/mcenter/common/meta"
)

func NewPolicySet() *PolicySet {
	return &PolicySet{
		Items: []*Policy{},
	}
}

func (s *PolicySet) Add(item *Policy) {
	s.Items = append(s.Items, item)
}

func (s *PolicySet) GetRoles() (roles []*role.Role) {
	for i := range s.Items {
		item := s.Items[i]
		roles = append(roles, item.Role)
	}
	return
}

func (s *PolicySet) RoleIds() (roleIds []string) {
	for i := range s.Items {
		item := s.Items[i]
		roleIds = append(roleIds, item.Spec.RoleId)
	}
	return
}

func (s *PolicySet) Len() uint64 {
	return uint64(len(s.Items))
}

func (s *PolicySet) SetRole(r *role.Role) {
	for i := range s.Items {
		item := s.Items[i]
		if item.Spec.RoleId == r.Meta.Id {
			item.Role = r
		}
	}
}

func NewDefaultPolicy() *Policy {
	return &Policy{
		Meta: meta.NewMeta(),
		Spec: NewCreatePolicyRequest(),
	}
}
