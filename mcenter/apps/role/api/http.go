package api

import (
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/role"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
)

type handler struct {
	service role.Service
}

var (
	h = &handler{}
)

func (h *handler) Config() error {
	h.service = app.GetInternalApp(role.AppName).(role.Service)
	return nil
}

func (h *handler) Name() string {
	return role.AppName
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{"角色"}
	ws.Route(ws.POST("/").To(h.CreateRole).
		Doc("创建角色").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(role.CreateRoleRequest{}).
		Metadata("service", "colh0e2mpgsgv37i2kl0").
		Writes(role.Role{}).
		Returns(200, "OK", role.Role{}))
	ws.Route(ws.POST("/list").To(h.QueryRole).
		Doc("查询角色列表").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Metadata("service", "colh0e2mpgsgv37i2kl0").
		Reads(role.QueryRoleRequest{}).
		Metadata("auth", true).
		Writes(role.RoleSet{}).
		Returns(200, "OK", role.RoleSet{}))
}

func init() {
	app.RegistryRESTfulApp(h)
}
