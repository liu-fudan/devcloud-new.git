package api

import (
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/role"
	"gitee.com/liu-fudan/devcloud-new/mcenter/common/errors"
	"github.com/emicklei/go-restful/v3"
)

func (h *handler) CreateRole(r *restful.Request, w *restful.Response) {
	in := role.NewCreateRoleRequest()
	err := r.ReadEntity(in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}
	user, err := h.service.CreateRole(r.Request.Context(), in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}

	w.WriteEntity(user)
}

func (h *handler) QueryRole(r *restful.Request, w *restful.Response) {
	in := role.NewQueryRoleRequest()
	err := r.ReadEntity(in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}
	user, err := h.service.QueryRole(r.Request.Context(), in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}

	w.WriteEntity(user)
}
