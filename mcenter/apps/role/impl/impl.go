package impl

import (
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/role"
	"gitee.com/liu-fudan/devcloud-new/mcenter/conf"
)

var svc = &impl{}

type impl struct {
	role.UnimplementedRPCServer
	col *mongo.Collection
}

func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection(role.AppName)
	return nil
}

func (i *impl) Name() string {
	return role.AppName
}

func (i *impl) Registry(server *grpc.Server) {
	role.RegisterRPCServer(server, i)
}

func init() {
	app.RegistryGrpcApp(svc)
	app.RegistryInternalApp(svc)
}
