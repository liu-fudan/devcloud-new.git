package impl_test

import (
	"testing"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/role"
)

func TestCreateRole(t *testing.T) {
	req := role.NewCreateRoleRequest()
	req.Name = "cmdb_manager"
	req.AddFeature(&role.Feature{
		ServiceId:  "service01",
		HttpMethod: "POST",
		HttpPath:   "cmdb/api/v1/resource",
	})
	ins, err := impl.CreateRole(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQueryRole(t *testing.T) {
	req := role.NewQueryRoleRequest()
	// req.AddRoleId("coh07lampgsi2t4kpq50")
	set, err := impl.QueryRole(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
