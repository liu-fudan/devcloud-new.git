package role

import (
	"context"

	"gitee.com/liu-fudan/fu-dan-tools-package/http/request"
)

const (
	AppName = "roles"
)

type Service interface {
	CreateRole(context.Context, *CreateRoleRequest) (*Role, error)
	RPCServer
}

func NewQueryRoleRequest() *QueryRoleRequest {
	return &QueryRoleRequest{Page: request.NewDefaultPageRequest()}
}

func (req *QueryRoleRequest) AddRoleId(ids ...string) {
	req.RoleIds = append(req.RoleIds, ids...)
}
