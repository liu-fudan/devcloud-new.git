package role

import "gitee.com/liu-fudan/devcloud-new/mcenter/common/meta"

func NewCreateRoleRequest() *CreateRoleRequest {
	return &CreateRoleRequest{
		Features: []*Feature{},
	}
}

func (req *CreateRoleRequest) AddFeature(f *Feature) {
	req.Features = append(req.Features, f)
}

func New(req *CreateRoleRequest) *Role {
	return &Role{
		Meta: meta.NewMeta(),
		Spec: req,
	}
}

func NewRoleSet() *RoleSet {
	return &RoleSet{
		Items: []*Role{},
	}
}

func (s *RoleSet) Add(item *Role) {
	s.Items = append(s.Items, item)
}

func NewDefaultRole() *Role {
	return New(NewCreateRoleRequest())
}

func (r *Role) HasFeatrue(serviceId, httpMethod, httpPath string) bool {
	for i := range r.Spec.Features {
		f := r.Spec.Features[i]
		if f.IsEqual(serviceId, httpMethod, httpPath) {
			return true
		}
	}
	return false
}

func (f *Feature) IsEqual(serviceId, httpMethod, httpPath string) bool {
	return f.ServiceId == serviceId &&
		f.HttpMethod == httpMethod &&
		f.HttpPath == httpPath
}
