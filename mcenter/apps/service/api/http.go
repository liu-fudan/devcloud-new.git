package api

import (
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/service"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
)

var h = &handler{}

type handler struct {
	service service.ServiceManager
}

func (h *handler) Config() error {
	h.service = app.GetInternalApp(service.AppName).(service.ServiceManager)
	return nil
}

func (h *handler) Name() string {
	return service.AppName
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{"登录"}
	ws.Route(ws.POST("/").To(h.CreateService).
		Doc("创建服务").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(service.CreateServiceRequest{}).
		Metadata("auth", true).
		Metadata("resource", "服务").
		Writes(service.Service{}).
		Returns(200, "OK", service.Service{}))
}

func init() {
	app.RegistryRESTfulApp(h)
}
