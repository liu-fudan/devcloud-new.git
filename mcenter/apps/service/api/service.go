package api

import (
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/service"
	"github.com/emicklei/go-restful/v3"
)

func (h *handler) CreateService(r *restful.Request, w *restful.Response) {
	in := service.NewDefaultCreateServiceRequest()
	err := r.ReadEntity(in)
	if err != nil {
		w.WriteEntity(err.Error())
		return
	}
	svr, err := h.service.CreateService(r.Request.Context(), in)
	if err != nil {
		w.WriteEntity(err.Error())
		return
	}
	w.WriteEntity(svr)
}
