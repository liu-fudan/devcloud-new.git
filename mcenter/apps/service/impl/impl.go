package impl

import (
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/service"
	"gitee.com/liu-fudan/devcloud-new/mcenter/conf"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

var (
	svc = &impl{}
)

type impl struct {
	service.UnimplementedRPCServer
	col *mongo.Collection
}

func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection(service.AppName)

	return nil
}

func (i *impl) Name() string {
	return service.AppName
}

func (i *impl) Registry(server *grpc.Server) {
	service.RegisterRPCServer(server, i)
}

func init() {
	app.RegistryGrpcApp(svc)
	app.RegistryInternalApp(svc)
}
