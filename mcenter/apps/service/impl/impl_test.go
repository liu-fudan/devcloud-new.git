package impl_test

import (
	"context"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/service"
	"gitee.com/liu-fudan/devcloud-new/mcenter/test/tools"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
)

var (
	impl service.ServiceManager

	ctx = context.Background()
)

// 加载ioc, 需要加载测试用例的配置
func init() {
	// ioc 初始化
	tools.DevelopmentSetup()

	// 从ioc里面取出需要别测试的对象
	impl = app.GetInternalApp(service.AppName).(service.ServiceManager)
}
