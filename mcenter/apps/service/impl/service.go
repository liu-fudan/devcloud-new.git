package impl

import (
	"context"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/service"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/service/provider/gitlab"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (i *impl) CreateService(ctx context.Context, in *service.CreateServiceRequest) (
	*service.Service, error) {
	ins := service.New(in)

	_, err := i.col.InsertOne(ctx, ins)
	if err != nil {
		return nil, err
	}

	return ins, nil
}

func (i *impl) DescribeService(ctx context.Context, in *service.DescribeServiceRequest) (
	*service.Service, error) {
	filer := bson.M{}
	switch in.DescribeBy {
	case service.DESCRIBE_BY_SERVICE_ID:
		filer["_id"] = in.DescribeVaule
	case service.DESCRIBE_BY_SERVICE_CREDENTAIL_ID:
		filer["credentail.client_id"] = in.DescribeVaule
	}

	ins := service.New(service.NewDefaultCreateServiceRequest())
	err := i.col.FindOne(ctx, filer).Decode(ins)
	if err != nil {
		return nil, err
	}

	return ins, nil
}

func (i *impl) QueryService(ctx context.Context, req *service.QueryServiceRequest) (
	*service.ServiceSet, error) {
	query := newQueryRequest(req)
	return i.query(ctx, query)
}

type queryRequest struct {
	*service.QueryServiceRequest
}

func newQueryRequest(r *service.QueryServiceRequest) *queryRequest {
	return &queryRequest{
		r,
	}
}

func (r *queryRequest) FindOptions() *options.FindOptions {
	pageSize := int64(r.Page.PageSize)
	skip := int64(r.Page.PageSize) * int64(r.Page.PageNumber-1)

	opt := &options.FindOptions{
		Sort: bson.D{
			{Key: "create_at", Value: -1},
		},
		Limit: &pageSize,
		Skip:  &skip,
	}

	return opt
}

func (r *queryRequest) FindFilter() bson.M {
	filter := bson.M{}

	if len(r.RepositorySshUrls) > 0 {
		filter["spec.repository.ssh_url"] = bson.M{"$in": r.RepositorySshUrls}
	}

	return filter
}

func (i *impl) query(ctx context.Context, req *queryRequest) (*service.ServiceSet, error) {
	resp, err := i.col.Find(ctx, req.FindFilter(), req.FindOptions())

	if err != nil {
		return nil, err
	}

	ServiceSet := service.NewServiceSet()
	// 循环
	for resp.Next(ctx) {
		ins := service.NewDefaultService()
		if err := resp.Decode(ins); err != nil {
			return nil, err
		}

		ServiceSet.Add(ins)
	}

	// count
	count, err := i.col.CountDocuments(ctx, req.FindFilter())
	if err != nil {
		return nil, err
	}
	ServiceSet.Total = count

	return ServiceSet, nil
}

func (i *impl) QueryGitlabProject(ctx context.Context, in *service.QueryGitlabProjectRequest) (
	*service.ServiceSet, error) {
	v4 := gitlab.NewGitlabV4(in.MakeConfig())
	pReq := gitlab.NewListProjectRequest()
	set, err := v4.Project().ListProjects(ctx, pReq)
	if err != nil {
		return nil, err
	}

	svcs := service.NewServiceSet()
	if set.Len() > 0 {
		gitSshUrls := set.GitSshUrls()
		query := service.NewQueryServiceRequest()
		query.RepositorySshUrls = gitSshUrls
		query.Page.PageSize = uint64(len(gitSshUrls))
		svcs, err = i.QueryService(ctx, query)
		if err != nil {
			return nil, err
		}

		for i := range set.Items {
			p := set.Items[i]
			svcs.UpdateFromGitProject(p, in.Token)
		}
	}

	return svcs, nil
}
