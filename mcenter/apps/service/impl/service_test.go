package impl_test

import (
	"os"
	"testing"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/service"
	"gitee.com/liu-fudan/devcloud-new/mcenter/test/tools"
)

func TestCreateService(t *testing.T) {
	req := &service.CreateServiceRequest{
		Domain:    "default",
		Namespace: "default",
		Name:      "mpaas",
	}

	ins, err := impl.CreateService(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}

func TestDescribeServiceByCrendentail(t *testing.T) {
	req := &service.DescribeServiceRequest{
		DescribeBy:    service.DESCRIBE_BY_SERVICE_CREDENTAIL_ID,
		DescribeVaule: "clslotto40255qbjo6ig",
	}

	ins, err := impl.DescribeService(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}

func TestDescribeServiceById(t *testing.T) {
	req := &service.DescribeServiceRequest{
		DescribeBy:    service.DESCRIBE_BY_SERVICE_ID,
		DescribeVaule: "cfsrgnh3n7pi7u2is87g",
	}

	ins, err := impl.DescribeService(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}

func TestQueryGitlabProject(t *testing.T) {
	req := service.NewQueryGitlabProjectRequest()
	req.Address = os.Getenv("GITLAB_ADDRESS")
	req.Token = os.Getenv("GITLAB_PRIVATE_TOKEN")
	set, err := impl.QueryGitlabProject(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(set))
}

func TestCreateServiceFromGitLab(t *testing.T) {
	req := service.NewCreateServiceRequest()
	// tools.MustReadJsonFile("./test/create_mcenter_service.json", req)
	// tools.MustReadJsonFile("./test/create_maudit_service.json", req)
	tools.MustReadJsonFile("./test/create_cmdb_service.json", req)
	req.Repository.Token = os.Getenv("GITLAB_PRIVATE_TOKEN")

	app, err := impl.CreateService(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(app)
}
