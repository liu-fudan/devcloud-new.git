package gitlab_test

import (
	"context"
	"testing"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/service/provider/gitlab"
	"github.com/infraboard/mcube/logger/zap"
)

var (
	v4  *gitlab.GitlabV4
	ctx = context.Background()

	ProjectID string = "44712580"
)

func TestListProject(t *testing.T) {
	req := gitlab.NewListProjectRequest()
	req.PageSize = 20
	req.PageNumer = 1
	req.Keywords = ""
	set, err := v4.Project().ListProjects(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

func init() {
	zap.DevelopmentSetup()
	conf, err := gitlab.LoadConfigFromEnv()
	if err != nil {
		panic(err)
	}
	v4 = gitlab.NewGitlabV4(conf)
}
