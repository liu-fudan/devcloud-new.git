package gitlab

import (
	"context"
	"strconv"

	"github.com/infraboard/mcube/client/rest"
)

func newProjectV4(gitlab *GitlabV4) *ProjectV4 {
	return &ProjectV4{
		client: gitlab.client.Group("projects"),
	}
}

type ProjectV4 struct {
	client *rest.RESTClient
}

// Get a list of all visible projects across GitLab for the authenticated user.
// 参考文档: https://docs.gitlab.com/ee/api/projects.html#list-all-projects
func (p *ProjectV4) ListProjects(ctx context.Context, in *ListProjectRequest) (*ProjectSet, error) {
	set := NewProjectSet()

	var total string
	err := p.client.
		Get("/").
		Param("owned", strconv.FormatBool(in.Owned)).
		Param("simple", strconv.FormatBool(in.Simple)).
		Param("page", in.PageNumerToString()).
		Param("per_page", in.PageSizeToString()).
		Param("order_by", "created_at").
		Param("sort", "desc").
		Param("search", in.Keywords).
		Do(ctx).
		Header(RESPONSE_HEADER_X_TOTAL, &total).
		Into(&set.Items)

	set.SetTotalFromString(total)
	if err != nil {
		return nil, err
	}
	return set, nil
}
