// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.21.9
// source: apps/service/pb/rpc.proto

package service

import (
	request "gitee.com/liu-fudan/fu-dan-tools-package/http/request"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type DESCRIBE_BY int32

const (
	DESCRIBE_BY_SERVICE_ID            DESCRIBE_BY = 0
	DESCRIBE_BY_SERVICE_CREDENTAIL_ID DESCRIBE_BY = 1
)

// Enum value maps for DESCRIBE_BY.
var (
	DESCRIBE_BY_name = map[int32]string{
		0: "SERVICE_ID",
		1: "SERVICE_CREDENTAIL_ID",
	}
	DESCRIBE_BY_value = map[string]int32{
		"SERVICE_ID":            0,
		"SERVICE_CREDENTAIL_ID": 1,
	}
)

func (x DESCRIBE_BY) Enum() *DESCRIBE_BY {
	p := new(DESCRIBE_BY)
	*p = x
	return p
}

func (x DESCRIBE_BY) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (DESCRIBE_BY) Descriptor() protoreflect.EnumDescriptor {
	return file_apps_service_pb_rpc_proto_enumTypes[0].Descriptor()
}

func (DESCRIBE_BY) Type() protoreflect.EnumType {
	return &file_apps_service_pb_rpc_proto_enumTypes[0]
}

func (x DESCRIBE_BY) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use DESCRIBE_BY.Descriptor instead.
func (DESCRIBE_BY) EnumDescriptor() ([]byte, []int) {
	return file_apps_service_pb_rpc_proto_rawDescGZIP(), []int{0}
}

type QueryServiceRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 分页参数
	Page *request.PageRequest `protobuf:"bytes,1,opt,name=page,proto3" json:"page,omitempty"`
	// 关键字查询
	Keywords string `protobuf:"bytes,2,opt,name=keywords,proto3" json:"keywords,omitempty"`
	// 服务代码SSH仓库地址
	// @gotags: json:"repository_ssh_urls" yaml:"repository_ssh_urls"
	RepositorySshUrls []string `protobuf:"bytes,3,rep,name=repository_ssh_urls,json=repositorySshUrls,proto3" json:"repository_ssh_urls" yaml:"repository_ssh_urls"`
}

func (x *QueryServiceRequest) Reset() {
	*x = QueryServiceRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_service_pb_rpc_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *QueryServiceRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*QueryServiceRequest) ProtoMessage() {}

func (x *QueryServiceRequest) ProtoReflect() protoreflect.Message {
	mi := &file_apps_service_pb_rpc_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use QueryServiceRequest.ProtoReflect.Descriptor instead.
func (*QueryServiceRequest) Descriptor() ([]byte, []int) {
	return file_apps_service_pb_rpc_proto_rawDescGZIP(), []int{0}
}

func (x *QueryServiceRequest) GetPage() *request.PageRequest {
	if x != nil {
		return x.Page
	}
	return nil
}

func (x *QueryServiceRequest) GetKeywords() string {
	if x != nil {
		return x.Keywords
	}
	return ""
}

func (x *QueryServiceRequest) GetRepositorySshUrls() []string {
	if x != nil {
		return x.RepositorySshUrls
	}
	return nil
}

type DescribeServiceRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 查询方式
	DescribeBy DESCRIBE_BY `protobuf:"varint,1,opt,name=describe_by,json=describeBy,proto3,enum=devcloud.service.DESCRIBE_BY" json:"describe_by,omitempty"`
	// 具体的值
	DescribeVaule string `protobuf:"bytes,2,opt,name=describe_vaule,json=describeVaule,proto3" json:"describe_vaule,omitempty"`
}

func (x *DescribeServiceRequest) Reset() {
	*x = DescribeServiceRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_service_pb_rpc_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DescribeServiceRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DescribeServiceRequest) ProtoMessage() {}

func (x *DescribeServiceRequest) ProtoReflect() protoreflect.Message {
	mi := &file_apps_service_pb_rpc_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DescribeServiceRequest.ProtoReflect.Descriptor instead.
func (*DescribeServiceRequest) Descriptor() ([]byte, []int) {
	return file_apps_service_pb_rpc_proto_rawDescGZIP(), []int{1}
}

func (x *DescribeServiceRequest) GetDescribeBy() DESCRIBE_BY {
	if x != nil {
		return x.DescribeBy
	}
	return DESCRIBE_BY_SERVICE_ID
}

func (x *DescribeServiceRequest) GetDescribeVaule() string {
	if x != nil {
		return x.DescribeVaule
	}
	return ""
}

type QueryGitlabProjectRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// gitlab服务地址
	// @gotags: json:"address"
	Address string `protobuf:"bytes,1,opt,name=address,proto3" json:"address"`
	// 访问Token
	// @gotags: json:"token" validate:"required"
	Token string `protobuf:"bytes,2,opt,name=token,proto3" json:"token" validate:"required"`
}

func (x *QueryGitlabProjectRequest) Reset() {
	*x = QueryGitlabProjectRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_service_pb_rpc_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *QueryGitlabProjectRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*QueryGitlabProjectRequest) ProtoMessage() {}

func (x *QueryGitlabProjectRequest) ProtoReflect() protoreflect.Message {
	mi := &file_apps_service_pb_rpc_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use QueryGitlabProjectRequest.ProtoReflect.Descriptor instead.
func (*QueryGitlabProjectRequest) Descriptor() ([]byte, []int) {
	return file_apps_service_pb_rpc_proto_rawDescGZIP(), []int{2}
}

func (x *QueryGitlabProjectRequest) GetAddress() string {
	if x != nil {
		return x.Address
	}
	return ""
}

func (x *QueryGitlabProjectRequest) GetToken() string {
	if x != nil {
		return x.Token
	}
	return ""
}

var File_apps_service_pb_rpc_proto protoreflect.FileDescriptor

var file_apps_service_pb_rpc_proto_rawDesc = []byte{
	0x0a, 0x19, 0x61, 0x70, 0x70, 0x73, 0x2f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2f, 0x70,
	0x62, 0x2f, 0x72, 0x70, 0x63, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x10, 0x64, 0x65, 0x76,
	0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x1a, 0x1d, 0x61,
	0x70, 0x70, 0x73, 0x2f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2f, 0x70, 0x62, 0x2f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x0f, 0x70, 0x61,
	0x67, 0x65, 0x2f, 0x70, 0x61, 0x67, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x9a, 0x01,
	0x0a, 0x13, 0x51, 0x75, 0x65, 0x72, 0x79, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x37, 0x0a, 0x04, 0x70, 0x61, 0x67, 0x65, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x23, 0x2e, 0x66, 0x75, 0x64, 0x61, 0x6e, 0x74, 0x6f, 0x6f, 0x6c, 0x73,
	0x70, 0x61, 0x63, 0x6b, 0x61, 0x67, 0x65, 0x2e, 0x70, 0x61, 0x67, 0x65, 0x2e, 0x50, 0x61, 0x67,
	0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52, 0x04, 0x70, 0x61, 0x67, 0x65, 0x12, 0x1a,
	0x0a, 0x08, 0x6b, 0x65, 0x79, 0x77, 0x6f, 0x72, 0x64, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x08, 0x6b, 0x65, 0x79, 0x77, 0x6f, 0x72, 0x64, 0x73, 0x12, 0x2e, 0x0a, 0x13, 0x72, 0x65,
	0x70, 0x6f, 0x73, 0x69, 0x74, 0x6f, 0x72, 0x79, 0x5f, 0x73, 0x73, 0x68, 0x5f, 0x75, 0x72, 0x6c,
	0x73, 0x18, 0x03, 0x20, 0x03, 0x28, 0x09, 0x52, 0x11, 0x72, 0x65, 0x70, 0x6f, 0x73, 0x69, 0x74,
	0x6f, 0x72, 0x79, 0x53, 0x73, 0x68, 0x55, 0x72, 0x6c, 0x73, 0x22, 0x7f, 0x0a, 0x16, 0x44, 0x65,
	0x73, 0x63, 0x72, 0x69, 0x62, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x3e, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x62, 0x65,
	0x5f, 0x62, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x1d, 0x2e, 0x64, 0x65, 0x76, 0x63,
	0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x44, 0x45, 0x53,
	0x43, 0x52, 0x49, 0x42, 0x45, 0x5f, 0x42, 0x59, 0x52, 0x0a, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69,
	0x62, 0x65, 0x42, 0x79, 0x12, 0x25, 0x0a, 0x0e, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x62, 0x65,
	0x5f, 0x76, 0x61, 0x75, 0x6c, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x64, 0x65,
	0x73, 0x63, 0x72, 0x69, 0x62, 0x65, 0x56, 0x61, 0x75, 0x6c, 0x65, 0x22, 0x4b, 0x0a, 0x19, 0x51,
	0x75, 0x65, 0x72, 0x79, 0x47, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63,
	0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x18, 0x0a, 0x07, 0x61, 0x64, 0x64, 0x72,
	0x65, 0x73, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65,
	0x73, 0x73, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x2a, 0x38, 0x0a, 0x0b, 0x44, 0x45, 0x53, 0x43,
	0x52, 0x49, 0x42, 0x45, 0x5f, 0x42, 0x59, 0x12, 0x0e, 0x0a, 0x0a, 0x53, 0x45, 0x52, 0x56, 0x49,
	0x43, 0x45, 0x5f, 0x49, 0x44, 0x10, 0x00, 0x12, 0x19, 0x0a, 0x15, 0x53, 0x45, 0x52, 0x56, 0x49,
	0x43, 0x45, 0x5f, 0x43, 0x52, 0x45, 0x44, 0x45, 0x4e, 0x54, 0x41, 0x49, 0x4c, 0x5f, 0x49, 0x44,
	0x10, 0x01, 0x32, 0x93, 0x02, 0x0a, 0x03, 0x52, 0x50, 0x43, 0x12, 0x53, 0x0a, 0x0c, 0x51, 0x75,
	0x65, 0x72, 0x79, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x25, 0x2e, 0x64, 0x65, 0x76,
	0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x51, 0x75,
	0x65, 0x72, 0x79, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x1c, 0x2e, 0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x53, 0x65, 0x74, 0x12,
	0x56, 0x0a, 0x0f, 0x44, 0x65, 0x73, 0x63, 0x72, 0x69, 0x62, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x12, 0x28, 0x2e, 0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x44, 0x65, 0x73, 0x63, 0x72, 0x69, 0x62, 0x65, 0x53, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x64,
	0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e,
	0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x5f, 0x0a, 0x12, 0x51, 0x75, 0x65, 0x72, 0x79,
	0x47, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x12, 0x2b, 0x2e,
	0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x2e, 0x51, 0x75, 0x65, 0x72, 0x79, 0x47, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x50, 0x72, 0x6f, 0x6a,
	0x65, 0x63, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1c, 0x2e, 0x64, 0x65, 0x76,
	0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x53, 0x65, 0x74, 0x42, 0x37, 0x5a, 0x35, 0x67, 0x69, 0x74, 0x65,
	0x65, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x6c, 0x69, 0x75, 0x2d, 0x66, 0x75, 0x64, 0x61, 0x6e, 0x2f,
	0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2d, 0x6e, 0x65, 0x77, 0x2f, 0x6d, 0x63, 0x65,
	0x6e, 0x74, 0x65, 0x72, 0x2f, 0x61, 0x70, 0x70, 0x73, 0x2f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_apps_service_pb_rpc_proto_rawDescOnce sync.Once
	file_apps_service_pb_rpc_proto_rawDescData = file_apps_service_pb_rpc_proto_rawDesc
)

func file_apps_service_pb_rpc_proto_rawDescGZIP() []byte {
	file_apps_service_pb_rpc_proto_rawDescOnce.Do(func() {
		file_apps_service_pb_rpc_proto_rawDescData = protoimpl.X.CompressGZIP(file_apps_service_pb_rpc_proto_rawDescData)
	})
	return file_apps_service_pb_rpc_proto_rawDescData
}

var file_apps_service_pb_rpc_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_apps_service_pb_rpc_proto_msgTypes = make([]protoimpl.MessageInfo, 3)
var file_apps_service_pb_rpc_proto_goTypes = []interface{}{
	(DESCRIBE_BY)(0),                  // 0: devcloud.service.DESCRIBE_BY
	(*QueryServiceRequest)(nil),       // 1: devcloud.service.QueryServiceRequest
	(*DescribeServiceRequest)(nil),    // 2: devcloud.service.DescribeServiceRequest
	(*QueryGitlabProjectRequest)(nil), // 3: devcloud.service.QueryGitlabProjectRequest
	(*request.PageRequest)(nil),       // 4: fudantoolspackage.page.PageRequest
	(*ServiceSet)(nil),                // 5: devcloud.service.ServiceSet
	(*Service)(nil),                   // 6: devcloud.service.Service
}
var file_apps_service_pb_rpc_proto_depIdxs = []int32{
	4, // 0: devcloud.service.QueryServiceRequest.page:type_name -> fudantoolspackage.page.PageRequest
	0, // 1: devcloud.service.DescribeServiceRequest.describe_by:type_name -> devcloud.service.DESCRIBE_BY
	1, // 2: devcloud.service.RPC.QueryService:input_type -> devcloud.service.QueryServiceRequest
	2, // 3: devcloud.service.RPC.DescribeService:input_type -> devcloud.service.DescribeServiceRequest
	3, // 4: devcloud.service.RPC.QueryGitlabProject:input_type -> devcloud.service.QueryGitlabProjectRequest
	5, // 5: devcloud.service.RPC.QueryService:output_type -> devcloud.service.ServiceSet
	6, // 6: devcloud.service.RPC.DescribeService:output_type -> devcloud.service.Service
	5, // 7: devcloud.service.RPC.QueryGitlabProject:output_type -> devcloud.service.ServiceSet
	5, // [5:8] is the sub-list for method output_type
	2, // [2:5] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_apps_service_pb_rpc_proto_init() }
func file_apps_service_pb_rpc_proto_init() {
	if File_apps_service_pb_rpc_proto != nil {
		return
	}
	file_apps_service_pb_service_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_apps_service_pb_rpc_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*QueryServiceRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_apps_service_pb_rpc_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DescribeServiceRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_apps_service_pb_rpc_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*QueryGitlabProjectRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_apps_service_pb_rpc_proto_rawDesc,
			NumEnums:      1,
			NumMessages:   3,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_apps_service_pb_rpc_proto_goTypes,
		DependencyIndexes: file_apps_service_pb_rpc_proto_depIdxs,
		EnumInfos:         file_apps_service_pb_rpc_proto_enumTypes,
		MessageInfos:      file_apps_service_pb_rpc_proto_msgTypes,
	}.Build()
	File_apps_service_pb_rpc_proto = out.File
	file_apps_service_pb_rpc_proto_rawDesc = nil
	file_apps_service_pb_rpc_proto_goTypes = nil
	file_apps_service_pb_rpc_proto_depIdxs = nil
}
