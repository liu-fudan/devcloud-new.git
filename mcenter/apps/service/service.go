package service

import (
	"time"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/domain"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/namespace"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/service/provider/gitlab"
	"gitee.com/liu-fudan/devcloud-new/mcenter/common/meta"
	request "gitee.com/liu-fudan/fu-dan-tools-package/http/request"
	"github.com/rs/xid"
)

func New(in *CreateServiceRequest) *Service {
	return &Service{
		Meta: meta.NewMeta(),
		Spec: in,
		Credentail: &Credentail{
			ClientId:     xid.New().String(),
			ClientSecret: xid.New().String(),
			CreateAt:     time.Now().Unix(),
		},
	}
}

func NewDefaultCreateServiceRequest() *CreateServiceRequest {
	return &CreateServiceRequest{}
}

func NewQueryGitlabProjectRequest() *QueryGitlabProjectRequest {
	return &QueryGitlabProjectRequest{}
}

func NewServiceSet() *ServiceSet {
	return &ServiceSet{
		Items: []*Service{},
	}
}

func NewDefaultService() *Service {
	return &Service{
		Spec: NewCreateServiceRequest(),
	}
}

func NewCreateServiceRequest() *CreateServiceRequest {
	return &CreateServiceRequest{
		Domain:     domain.DEFAULT_DOMAIN,
		Namespace:  namespace.DEFAULT_NAMESPACE,
		Repository: NewRepository(),
		Labels:     map[string]string{},
	}
}

func NewRepository() *Repository {
	return &Repository{
		EnableHook: true,
	}
}

func (s *ServiceSet) Add(item *Service) {
	s.Items = append(s.Items, item)
}

func NewQueryServiceRequest() *QueryServiceRequest {
	return &QueryServiceRequest{
		Page: request.NewDefaultPageRequest(),
	}
}

func (req *QueryGitlabProjectRequest) MakeConfig() *gitlab.Config {
	conf := gitlab.NewDefaultConfig()
	if req.Address != "" {
		conf.Address = req.Address
	}
	conf.PrivateToken = req.Token
	return conf
}

func (s *ServiceSet) UpdateFromGitProject(p *gitlab.Project, tk string) {
	svc := s.GetServiceByGitSshUrl(p.GitSshUrl)
	if svc == nil {
		svc = NewServiceFromProject(p)
		svc.Spec.Repository.Token = tk
		s.Add(svc)
	}
}

func (s *ServiceSet) GetServiceByGitSshUrl(gitSshUrl string) *Service {
	for i := range s.Items {
		item := s.Items[i]
		if item.GetRepositorySshUrl() == gitSshUrl {
			return item
		}
	}

	return nil
}

func (s *Service) GetRepositorySshUrl() string {
	if s.Spec.Repository != nil {
		return s.Spec.Repository.SshUrl
	}

	return ""
}

func NewDescribeServiceRequest() *DescribeServiceRequest {
	return &DescribeServiceRequest{}
}

func NewServiceFromProject(p *gitlab.Project) *Service {
	svc := NewDefaultService()
	spec := svc.Spec
	spec.Name = p.Name
	spec.Repository.ProjectId = p.IdToString()
	spec.Repository.SshUrl = p.GitSshUrl
	spec.Repository.HttpUrl = p.GitHttpUrl
	spec.Repository.Namespace = p.NamespacePath
	spec.Repository.WebUrl = p.WebURL
	spec.Repository.CreatedAt = p.CreatedAt.Unix()
	spec.Repository.EnableHook = true
	spec.Repository.HookConfig = gitlab.NewGitLabWebHook(
		"自动填充服务的Id",
	).ToJson()
	return svc
}
