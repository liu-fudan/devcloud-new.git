// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.21.9
// source: apps/service/pb/service.proto

package service

import (
	meta "gitee.com/liu-fudan/devcloud-new/mcenter/common/meta"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type LANGUAGE int32

const (
	LANGUAGE_JAVA        LANGUAGE = 0
	LANGUAGE_JAVASCRIPT  LANGUAGE = 1
	LANGUAGE_GOLANG      LANGUAGE = 2
	LANGUAGE_GO          LANGUAGE = 3
	LANGUAGE_PYTHON      LANGUAGE = 4
	LANGUAGE_C_SHARP     LANGUAGE = 5
	LANGUAGE_C           LANGUAGE = 6
	LANGUAGE_C_PLUS_PLUS LANGUAGE = 7
	LANGUAGE_SHELL       LANGUAGE = 8
	LANGUAGE_POWER_SHELL LANGUAGE = 9
)

// Enum value maps for LANGUAGE.
var (
	LANGUAGE_name = map[int32]string{
		0: "JAVA",
		1: "JAVASCRIPT",
		2: "GOLANG",
		3: "GO",
		4: "PYTHON",
		5: "C_SHARP",
		6: "C",
		7: "C_PLUS_PLUS",
		8: "SHELL",
		9: "POWER_SHELL",
	}
	LANGUAGE_value = map[string]int32{
		"JAVA":        0,
		"JAVASCRIPT":  1,
		"GOLANG":      2,
		"GO":          3,
		"PYTHON":      4,
		"C_SHARP":     5,
		"C":           6,
		"C_PLUS_PLUS": 7,
		"SHELL":       8,
		"POWER_SHELL": 9,
	}
)

func (x LANGUAGE) Enum() *LANGUAGE {
	p := new(LANGUAGE)
	*p = x
	return p
}

func (x LANGUAGE) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (LANGUAGE) Descriptor() protoreflect.EnumDescriptor {
	return file_apps_service_pb_service_proto_enumTypes[0].Descriptor()
}

func (LANGUAGE) Type() protoreflect.EnumType {
	return &file_apps_service_pb_service_proto_enumTypes[0]
}

func (x LANGUAGE) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use LANGUAGE.Descriptor instead.
func (LANGUAGE) EnumDescriptor() ([]byte, []int) {
	return file_apps_service_pb_service_proto_rawDescGZIP(), []int{0}
}

// SCM_TYPE 源码仓库类型
type SCM_PROVIDER int32

const (
	// gitlab
	SCM_PROVIDER_GITLAB SCM_PROVIDER = 0
	// github
	SCM_PROVIDER_GITHUB SCM_PROVIDER = 1
	// coding.net
	SCM_PROVIDER_CODING SCM_PROVIDER = 2
)

// Enum value maps for SCM_PROVIDER.
var (
	SCM_PROVIDER_name = map[int32]string{
		0: "GITLAB",
		1: "GITHUB",
		2: "CODING",
	}
	SCM_PROVIDER_value = map[string]int32{
		"GITLAB": 0,
		"GITHUB": 1,
		"CODING": 2,
	}
)

func (x SCM_PROVIDER) Enum() *SCM_PROVIDER {
	p := new(SCM_PROVIDER)
	*p = x
	return p
}

func (x SCM_PROVIDER) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (SCM_PROVIDER) Descriptor() protoreflect.EnumDescriptor {
	return file_apps_service_pb_service_proto_enumTypes[1].Descriptor()
}

func (SCM_PROVIDER) Type() protoreflect.EnumType {
	return &file_apps_service_pb_service_proto_enumTypes[1]
}

func (x SCM_PROVIDER) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use SCM_PROVIDER.Descriptor instead.
func (SCM_PROVIDER) EnumDescriptor() ([]byte, []int) {
	return file_apps_service_pb_service_proto_rawDescGZIP(), []int{1}
}

// 服务集合
type ServiceSet struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 服务总数量
	// @gotags: json:"total"
	Total int64 `protobuf:"varint,1,opt,name=total,proto3" json:"total"`
	// 分页的服务列表
	// @gotags: json:"items"
	Items []*Service `protobuf:"bytes,2,rep,name=items,proto3" json:"items"`
}

func (x *ServiceSet) Reset() {
	*x = ServiceSet{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_service_pb_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ServiceSet) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ServiceSet) ProtoMessage() {}

func (x *ServiceSet) ProtoReflect() protoreflect.Message {
	mi := &file_apps_service_pb_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ServiceSet.ProtoReflect.Descriptor instead.
func (*ServiceSet) Descriptor() ([]byte, []int) {
	return file_apps_service_pb_service_proto_rawDescGZIP(), []int{0}
}

func (x *ServiceSet) GetTotal() int64 {
	if x != nil {
		return x.Total
	}
	return 0
}

func (x *ServiceSet) GetItems() []*Service {
	if x != nil {
		return x.Items
	}
	return nil
}

type Service struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 对象元数据
	// @gotags: bson:",inline" json:"meta"
	Meta *meta.Meta `protobuf:"bytes,1,opt,name=meta,proto3" json:"meta" bson:",inline"`
	// 创建服务的请求 {"spec": {"username": ""}}  -->inline-- {"username": ""}
	// @gotags: bson:",inline" json:"spec"
	Spec *CreateServiceRequest `protobuf:"bytes,15,opt,name=spec,proto3" json:"spec" bson:",inline"`
	// 服务凭证, 用于校验服务合法性
	// @gotags: bson:"credentail" json:"credentail"
	Credentail *Credentail `protobuf:"bytes,2,opt,name=credentail,proto3" json:"credentail" bson:"credentail"`
}

func (x *Service) Reset() {
	*x = Service{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_service_pb_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Service) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Service) ProtoMessage() {}

func (x *Service) ProtoReflect() protoreflect.Message {
	mi := &file_apps_service_pb_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Service.ProtoReflect.Descriptor instead.
func (*Service) Descriptor() ([]byte, []int) {
	return file_apps_service_pb_service_proto_rawDescGZIP(), []int{1}
}

func (x *Service) GetMeta() *meta.Meta {
	if x != nil {
		return x.Meta
	}
	return nil
}

func (x *Service) GetSpec() *CreateServiceRequest {
	if x != nil {
		return x.Spec
	}
	return nil
}

func (x *Service) GetCredentail() *Credentail {
	if x != nil {
		return x.Credentail
	}
	return nil
}

type Credentail struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 客户端id
	// @gotags: bson:"client_id" json:"client_id"
	ClientId string `protobuf:"bytes,1,opt,name=client_id,json=clientId,proto3" json:"client_id" bson:"client_id"`
	// 客户端密码
	// @gotags: bson:"client_secret" json:"client_secret"
	ClientSecret string `protobuf:"bytes,2,opt,name=client_secret,json=clientSecret,proto3" json:"client_secret" bson:"client_secret"`
	// 创建时间
	// @gotags: bson:"create_at" json:"create_at"
	CreateAt int64 `protobuf:"varint,3,opt,name=create_at,json=createAt,proto3" json:"create_at" bson:"create_at"`
}

func (x *Credentail) Reset() {
	*x = Credentail{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_service_pb_service_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Credentail) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Credentail) ProtoMessage() {}

func (x *Credentail) ProtoReflect() protoreflect.Message {
	mi := &file_apps_service_pb_service_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Credentail.ProtoReflect.Descriptor instead.
func (*Credentail) Descriptor() ([]byte, []int) {
	return file_apps_service_pb_service_proto_rawDescGZIP(), []int{2}
}

func (x *Credentail) GetClientId() string {
	if x != nil {
		return x.ClientId
	}
	return ""
}

func (x *Credentail) GetClientSecret() string {
	if x != nil {
		return x.ClientSecret
	}
	return ""
}

func (x *Credentail) GetCreateAt() int64 {
	if x != nil {
		return x.CreateAt
	}
	return 0
}

type CreateServiceRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 服务所属的Domain(公司或者组织名字)
	// @gotags: bson:"domain" json:"domain" validate:"required"
	Domain string `protobuf:"bytes,1,opt,name=domain,proto3" json:"domain" bson:"domain" validate:"required"`
	// 服务所属空间(项目)
	// @gotags: bson:"namespace" json:"namespace" validate:"required"
	Namespace string `protobuf:"bytes,2,opt,name=namespace,proto3" json:"namespace" bson:"namespace" validate:"required"`
	// 服务名称
	// @gotags: bson:"name" json:"name" validate:"required"
	Name string `protobuf:"bytes,3,opt,name=name,proto3" json:"name" bson:"name" validate:"required"`
	// 仓库地址
	// @gotags: bson:"repository" json:"repository"
	Repository *Repository `protobuf:"bytes,4,opt,name=repository,proto3" json:"repository" bson:"repository"`
	// 服务标签
	// @gotags: bson:"labels" json:"labels"
	Labels map[string]string `protobuf:"bytes,5,rep,name=labels,proto3" json:"labels" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3" bson:"labels"`
}

func (x *CreateServiceRequest) Reset() {
	*x = CreateServiceRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_service_pb_service_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateServiceRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateServiceRequest) ProtoMessage() {}

func (x *CreateServiceRequest) ProtoReflect() protoreflect.Message {
	mi := &file_apps_service_pb_service_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateServiceRequest.ProtoReflect.Descriptor instead.
func (*CreateServiceRequest) Descriptor() ([]byte, []int) {
	return file_apps_service_pb_service_proto_rawDescGZIP(), []int{3}
}

func (x *CreateServiceRequest) GetDomain() string {
	if x != nil {
		return x.Domain
	}
	return ""
}

func (x *CreateServiceRequest) GetNamespace() string {
	if x != nil {
		return x.Namespace
	}
	return ""
}

func (x *CreateServiceRequest) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *CreateServiceRequest) GetRepository() *Repository {
	if x != nil {
		return x.Repository
	}
	return nil
}

func (x *CreateServiceRequest) GetLabels() map[string]string {
	if x != nil {
		return x.Labels
	}
	return nil
}

// 服务仓库信息
type Repository struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 仓库提供商
	// @gotags: bson:"provider" json:"provider"
	Provider SCM_PROVIDER `protobuf:"varint,1,opt,name=provider,proto3,enum=devcloud.service.SCM_PROVIDER" json:"provider" bson:"provider"`
	// token 操作仓库, 比如设置回调
	// @gotags: bson:"token" json:"token,omitempty"
	Token string `protobuf:"bytes,2,opt,name=token,proto3" json:"token,omitempty" bson:"token"`
	// 仓库对应的项目Id
	// @gotags: bson:"project_id" json:"project_id"
	ProjectId string `protobuf:"bytes,3,opt,name=project_id,json=projectId,proto3" json:"project_id" bson:"project_id"`
	// 仓库对应空间
	// @gotags: bson:"namespace" json:"namespace"
	Namespace string `protobuf:"bytes,9,opt,name=namespace,proto3" json:"namespace" bson:"namespace"`
	// 仓库web url地址
	// @gotags: bson:"web_url" json:"web_url"
	WebUrl string `protobuf:"bytes,10,opt,name=web_url,json=webUrl,proto3" json:"web_url" bson:"web_url"`
	// 仓库ssh url地址
	// @gotags: bson:"ssh_url" json:"ssh_url"
	SshUrl string `protobuf:"bytes,4,opt,name=ssh_url,json=sshUrl,proto3" json:"ssh_url" bson:"ssh_url"`
	// 仓库http url地址
	// @gotags: bson:"http_url" json:"http_url"
	HttpUrl string `protobuf:"bytes,5,opt,name=http_url,json=httpUrl,proto3" json:"http_url" bson:"http_url"`
	// 源代码使用的编程语言, 构建时, 不同语言有不同的构建环境
	// @gotags: bson:"language" json:"language"
	Language *LANGUAGE `protobuf:"varint,6,opt,name=language,proto3,enum=devcloud.service.LANGUAGE,oneof" json:"language" bson:"language"`
	// 开启Hook设置
	// @gotags: bson:"enable_hook" json:"enable_hook"
	EnableHook bool `protobuf:"varint,7,opt,name=enable_hook,json=enableHook,proto3" json:"enable_hook" bson:"enable_hook"`
	// Hook设置
	// @gotags: bson:"hook_config" json:"hook_config"
	HookConfig string `protobuf:"bytes,8,opt,name=hook_config,json=hookConfig,proto3" json:"hook_config" bson:"hook_config"`
	// scm设置Hook后返回的id, 用于删除应用时，取消hook使用
	// @gotags: bson:"hook_id" json:"hook_id"
	HookId string `protobuf:"bytes,11,opt,name=hook_id,json=hookId,proto3" json:"hook_id" bson:"hook_id"`
	// 仓库的创建时间
	// @gotags: bson:"created_at" json:"created_at"
	CreatedAt int64 `protobuf:"varint,12,opt,name=created_at,json=createdAt,proto3" json:"created_at" bson:"created_at"`
}

func (x *Repository) Reset() {
	*x = Repository{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_service_pb_service_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Repository) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Repository) ProtoMessage() {}

func (x *Repository) ProtoReflect() protoreflect.Message {
	mi := &file_apps_service_pb_service_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Repository.ProtoReflect.Descriptor instead.
func (*Repository) Descriptor() ([]byte, []int) {
	return file_apps_service_pb_service_proto_rawDescGZIP(), []int{4}
}

func (x *Repository) GetProvider() SCM_PROVIDER {
	if x != nil {
		return x.Provider
	}
	return SCM_PROVIDER_GITLAB
}

func (x *Repository) GetToken() string {
	if x != nil {
		return x.Token
	}
	return ""
}

func (x *Repository) GetProjectId() string {
	if x != nil {
		return x.ProjectId
	}
	return ""
}

func (x *Repository) GetNamespace() string {
	if x != nil {
		return x.Namespace
	}
	return ""
}

func (x *Repository) GetWebUrl() string {
	if x != nil {
		return x.WebUrl
	}
	return ""
}

func (x *Repository) GetSshUrl() string {
	if x != nil {
		return x.SshUrl
	}
	return ""
}

func (x *Repository) GetHttpUrl() string {
	if x != nil {
		return x.HttpUrl
	}
	return ""
}

func (x *Repository) GetLanguage() LANGUAGE {
	if x != nil && x.Language != nil {
		return *x.Language
	}
	return LANGUAGE_JAVA
}

func (x *Repository) GetEnableHook() bool {
	if x != nil {
		return x.EnableHook
	}
	return false
}

func (x *Repository) GetHookConfig() string {
	if x != nil {
		return x.HookConfig
	}
	return ""
}

func (x *Repository) GetHookId() string {
	if x != nil {
		return x.HookId
	}
	return ""
}

func (x *Repository) GetCreatedAt() int64 {
	if x != nil {
		return x.CreatedAt
	}
	return 0
}

var File_apps_service_pb_service_proto protoreflect.FileDescriptor

var file_apps_service_pb_service_proto_rawDesc = []byte{
	0x0a, 0x1d, 0x61, 0x70, 0x70, 0x73, 0x2f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2f, 0x70,
	0x62, 0x2f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x10, 0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x1a, 0x16, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2f, 0x6d, 0x65, 0x74, 0x61, 0x2f, 0x6d,
	0x65, 0x74, 0x61, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x53, 0x0a, 0x0a, 0x53, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x53, 0x65, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x6f, 0x74, 0x61, 0x6c,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x12, 0x2f, 0x0a,
	0x05, 0x69, 0x74, 0x65, 0x6d, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x19, 0x2e, 0x64,
	0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e,
	0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x52, 0x05, 0x69, 0x74, 0x65, 0x6d, 0x73, 0x22, 0xac,
	0x01, 0x0a, 0x07, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x27, 0x0a, 0x04, 0x6d, 0x65,
	0x74, 0x61, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x13, 0x2e, 0x64, 0x65, 0x76, 0x63, 0x6c,
	0x6f, 0x75, 0x64, 0x2e, 0x6d, 0x65, 0x74, 0x61, 0x2e, 0x4d, 0x65, 0x74, 0x61, 0x52, 0x04, 0x6d,
	0x65, 0x74, 0x61, 0x12, 0x3a, 0x0a, 0x04, 0x73, 0x70, 0x65, 0x63, 0x18, 0x0f, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x26, 0x2e, 0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52, 0x04, 0x73, 0x70, 0x65, 0x63, 0x12,
	0x3c, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x64, 0x65, 0x6e, 0x74, 0x61, 0x69, 0x6c, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x43, 0x72, 0x65, 0x64, 0x65, 0x6e, 0x74, 0x61, 0x69,
	0x6c, 0x52, 0x0a, 0x63, 0x72, 0x65, 0x64, 0x65, 0x6e, 0x74, 0x61, 0x69, 0x6c, 0x22, 0x6b, 0x0a,
	0x0a, 0x43, 0x72, 0x65, 0x64, 0x65, 0x6e, 0x74, 0x61, 0x69, 0x6c, 0x12, 0x1b, 0x0a, 0x09, 0x63,
	0x6c, 0x69, 0x65, 0x6e, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08,
	0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x49, 0x64, 0x12, 0x23, 0x0a, 0x0d, 0x63, 0x6c, 0x69, 0x65,
	0x6e, 0x74, 0x5f, 0x73, 0x65, 0x63, 0x72, 0x65, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0c, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x53, 0x65, 0x63, 0x72, 0x65, 0x74, 0x12, 0x1b, 0x0a,
	0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x5f, 0x61, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x08, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x41, 0x74, 0x22, 0xa5, 0x02, 0x0a, 0x14, 0x43,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x64, 0x6f, 0x6d, 0x61, 0x69, 0x6e, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x06, 0x64, 0x6f, 0x6d, 0x61, 0x69, 0x6e, 0x12, 0x1c, 0x0a, 0x09, 0x6e,
	0x61, 0x6d, 0x65, 0x73, 0x70, 0x61, 0x63, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09,
	0x6e, 0x61, 0x6d, 0x65, 0x73, 0x70, 0x61, 0x63, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d,
	0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x3c, 0x0a,
	0x0a, 0x72, 0x65, 0x70, 0x6f, 0x73, 0x69, 0x74, 0x6f, 0x72, 0x79, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x1c, 0x2e, 0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2e, 0x52, 0x65, 0x70, 0x6f, 0x73, 0x69, 0x74, 0x6f, 0x72, 0x79, 0x52,
	0x0a, 0x72, 0x65, 0x70, 0x6f, 0x73, 0x69, 0x74, 0x6f, 0x72, 0x79, 0x12, 0x4a, 0x0a, 0x06, 0x6c,
	0x61, 0x62, 0x65, 0x6c, 0x73, 0x18, 0x05, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x32, 0x2e, 0x64, 0x65,
	0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x43,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x2e, 0x4c, 0x61, 0x62, 0x65, 0x6c, 0x73, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x52,
	0x06, 0x6c, 0x61, 0x62, 0x65, 0x6c, 0x73, 0x1a, 0x39, 0x0a, 0x0b, 0x4c, 0x61, 0x62, 0x65, 0x6c,
	0x73, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x03, 0x6b, 0x65, 0x79, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75,
	0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x3a, 0x02,
	0x38, 0x01, 0x22, 0xac, 0x03, 0x0a, 0x0a, 0x52, 0x65, 0x70, 0x6f, 0x73, 0x69, 0x74, 0x6f, 0x72,
	0x79, 0x12, 0x3a, 0x0a, 0x08, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x64, 0x65, 0x72, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x0e, 0x32, 0x1e, 0x2e, 0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x43, 0x4d, 0x5f, 0x50, 0x52, 0x4f, 0x56, 0x49,
	0x44, 0x45, 0x52, 0x52, 0x08, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x64, 0x65, 0x72, 0x12, 0x14, 0x0a,
	0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x74, 0x6f,
	0x6b, 0x65, 0x6e, 0x12, 0x1d, 0x0a, 0x0a, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x5f, 0x69,
	0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74,
	0x49, 0x64, 0x12, 0x1c, 0x0a, 0x09, 0x6e, 0x61, 0x6d, 0x65, 0x73, 0x70, 0x61, 0x63, 0x65, 0x18,
	0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x6e, 0x61, 0x6d, 0x65, 0x73, 0x70, 0x61, 0x63, 0x65,
	0x12, 0x17, 0x0a, 0x07, 0x77, 0x65, 0x62, 0x5f, 0x75, 0x72, 0x6c, 0x18, 0x0a, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x06, 0x77, 0x65, 0x62, 0x55, 0x72, 0x6c, 0x12, 0x17, 0x0a, 0x07, 0x73, 0x73, 0x68,
	0x5f, 0x75, 0x72, 0x6c, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x73, 0x68, 0x55,
	0x72, 0x6c, 0x12, 0x19, 0x0a, 0x08, 0x68, 0x74, 0x74, 0x70, 0x5f, 0x75, 0x72, 0x6c, 0x18, 0x05,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x68, 0x74, 0x74, 0x70, 0x55, 0x72, 0x6c, 0x12, 0x3b, 0x0a,
	0x08, 0x6c, 0x61, 0x6e, 0x67, 0x75, 0x61, 0x67, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x0e, 0x32,
	0x1a, 0x2e, 0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x2e, 0x4c, 0x41, 0x4e, 0x47, 0x55, 0x41, 0x47, 0x45, 0x48, 0x00, 0x52, 0x08, 0x6c,
	0x61, 0x6e, 0x67, 0x75, 0x61, 0x67, 0x65, 0x88, 0x01, 0x01, 0x12, 0x1f, 0x0a, 0x0b, 0x65, 0x6e,
	0x61, 0x62, 0x6c, 0x65, 0x5f, 0x68, 0x6f, 0x6f, 0x6b, 0x18, 0x07, 0x20, 0x01, 0x28, 0x08, 0x52,
	0x0a, 0x65, 0x6e, 0x61, 0x62, 0x6c, 0x65, 0x48, 0x6f, 0x6f, 0x6b, 0x12, 0x1f, 0x0a, 0x0b, 0x68,
	0x6f, 0x6f, 0x6b, 0x5f, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0a, 0x68, 0x6f, 0x6f, 0x6b, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x12, 0x17, 0x0a, 0x07,
	0x68, 0x6f, 0x6f, 0x6b, 0x5f, 0x69, 0x64, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x68,
	0x6f, 0x6f, 0x6b, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64,
	0x5f, 0x61, 0x74, 0x18, 0x0c, 0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x64, 0x41, 0x74, 0x42, 0x0b, 0x0a, 0x09, 0x5f, 0x6c, 0x61, 0x6e, 0x67, 0x75, 0x61, 0x67,
	0x65, 0x2a, 0x85, 0x01, 0x0a, 0x08, 0x4c, 0x41, 0x4e, 0x47, 0x55, 0x41, 0x47, 0x45, 0x12, 0x08,
	0x0a, 0x04, 0x4a, 0x41, 0x56, 0x41, 0x10, 0x00, 0x12, 0x0e, 0x0a, 0x0a, 0x4a, 0x41, 0x56, 0x41,
	0x53, 0x43, 0x52, 0x49, 0x50, 0x54, 0x10, 0x01, 0x12, 0x0a, 0x0a, 0x06, 0x47, 0x4f, 0x4c, 0x41,
	0x4e, 0x47, 0x10, 0x02, 0x12, 0x06, 0x0a, 0x02, 0x47, 0x4f, 0x10, 0x03, 0x12, 0x0a, 0x0a, 0x06,
	0x50, 0x59, 0x54, 0x48, 0x4f, 0x4e, 0x10, 0x04, 0x12, 0x0b, 0x0a, 0x07, 0x43, 0x5f, 0x53, 0x48,
	0x41, 0x52, 0x50, 0x10, 0x05, 0x12, 0x05, 0x0a, 0x01, 0x43, 0x10, 0x06, 0x12, 0x0f, 0x0a, 0x0b,
	0x43, 0x5f, 0x50, 0x4c, 0x55, 0x53, 0x5f, 0x50, 0x4c, 0x55, 0x53, 0x10, 0x07, 0x12, 0x09, 0x0a,
	0x05, 0x53, 0x48, 0x45, 0x4c, 0x4c, 0x10, 0x08, 0x12, 0x0f, 0x0a, 0x0b, 0x50, 0x4f, 0x57, 0x45,
	0x52, 0x5f, 0x53, 0x48, 0x45, 0x4c, 0x4c, 0x10, 0x09, 0x2a, 0x32, 0x0a, 0x0c, 0x53, 0x43, 0x4d,
	0x5f, 0x50, 0x52, 0x4f, 0x56, 0x49, 0x44, 0x45, 0x52, 0x12, 0x0a, 0x0a, 0x06, 0x47, 0x49, 0x54,
	0x4c, 0x41, 0x42, 0x10, 0x00, 0x12, 0x0a, 0x0a, 0x06, 0x47, 0x49, 0x54, 0x48, 0x55, 0x42, 0x10,
	0x01, 0x12, 0x0a, 0x0a, 0x06, 0x43, 0x4f, 0x44, 0x49, 0x4e, 0x47, 0x10, 0x02, 0x42, 0x37, 0x5a,
	0x35, 0x67, 0x69, 0x74, 0x65, 0x65, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x6c, 0x69, 0x75, 0x2d, 0x66,
	0x75, 0x64, 0x61, 0x6e, 0x2f, 0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2d, 0x6e, 0x65,
	0x77, 0x2f, 0x6d, 0x63, 0x65, 0x6e, 0x74, 0x65, 0x72, 0x2f, 0x61, 0x70, 0x70, 0x73, 0x2f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_apps_service_pb_service_proto_rawDescOnce sync.Once
	file_apps_service_pb_service_proto_rawDescData = file_apps_service_pb_service_proto_rawDesc
)

func file_apps_service_pb_service_proto_rawDescGZIP() []byte {
	file_apps_service_pb_service_proto_rawDescOnce.Do(func() {
		file_apps_service_pb_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_apps_service_pb_service_proto_rawDescData)
	})
	return file_apps_service_pb_service_proto_rawDescData
}

var file_apps_service_pb_service_proto_enumTypes = make([]protoimpl.EnumInfo, 2)
var file_apps_service_pb_service_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_apps_service_pb_service_proto_goTypes = []interface{}{
	(LANGUAGE)(0),                // 0: devcloud.service.LANGUAGE
	(SCM_PROVIDER)(0),            // 1: devcloud.service.SCM_PROVIDER
	(*ServiceSet)(nil),           // 2: devcloud.service.ServiceSet
	(*Service)(nil),              // 3: devcloud.service.Service
	(*Credentail)(nil),           // 4: devcloud.service.Credentail
	(*CreateServiceRequest)(nil), // 5: devcloud.service.CreateServiceRequest
	(*Repository)(nil),           // 6: devcloud.service.Repository
	nil,                          // 7: devcloud.service.CreateServiceRequest.LabelsEntry
	(*meta.Meta)(nil),            // 8: devcloud.meta.Meta
}
var file_apps_service_pb_service_proto_depIdxs = []int32{
	3, // 0: devcloud.service.ServiceSet.items:type_name -> devcloud.service.Service
	8, // 1: devcloud.service.Service.meta:type_name -> devcloud.meta.Meta
	5, // 2: devcloud.service.Service.spec:type_name -> devcloud.service.CreateServiceRequest
	4, // 3: devcloud.service.Service.credentail:type_name -> devcloud.service.Credentail
	6, // 4: devcloud.service.CreateServiceRequest.repository:type_name -> devcloud.service.Repository
	7, // 5: devcloud.service.CreateServiceRequest.labels:type_name -> devcloud.service.CreateServiceRequest.LabelsEntry
	1, // 6: devcloud.service.Repository.provider:type_name -> devcloud.service.SCM_PROVIDER
	0, // 7: devcloud.service.Repository.language:type_name -> devcloud.service.LANGUAGE
	8, // [8:8] is the sub-list for method output_type
	8, // [8:8] is the sub-list for method input_type
	8, // [8:8] is the sub-list for extension type_name
	8, // [8:8] is the sub-list for extension extendee
	0, // [0:8] is the sub-list for field type_name
}

func init() { file_apps_service_pb_service_proto_init() }
func file_apps_service_pb_service_proto_init() {
	if File_apps_service_pb_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_apps_service_pb_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ServiceSet); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_apps_service_pb_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Service); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_apps_service_pb_service_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Credentail); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_apps_service_pb_service_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateServiceRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_apps_service_pb_service_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Repository); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	file_apps_service_pb_service_proto_msgTypes[4].OneofWrappers = []interface{}{}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_apps_service_pb_service_proto_rawDesc,
			NumEnums:      2,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_apps_service_pb_service_proto_goTypes,
		DependencyIndexes: file_apps_service_pb_service_proto_depIdxs,
		EnumInfos:         file_apps_service_pb_service_proto_enumTypes,
		MessageInfos:      file_apps_service_pb_service_proto_msgTypes,
	}.Build()
	File_apps_service_pb_service_proto = out.File
	file_apps_service_pb_service_proto_rawDesc = nil
	file_apps_service_pb_service_proto_goTypes = nil
	file_apps_service_pb_service_proto_depIdxs = nil
}
