package api

import (
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/token"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
)

var h = &handler{}

type handler struct {
	service token.Service
}

func (h *handler) Config() error {
	h.service = app.GetInternalApp(token.AppName).(token.Service)
	return nil
}

func (h *handler) Name() string {
	return token.AppName
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{"登录"}
	ws.Route(ws.POST("/").To(h.IssueToken).
		Doc("颁发令牌").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(token.IssueTokenRequest{}).
		Metadata("auth", false).
		Metadata("resource", "令牌").
		Metadata("service", "colh0e2mpgsgv37i2kl0").
		Writes(token.Token{}).
		Returns(200, "OK", token.Token{}))
}

func init() {
	app.RegistryRESTfulApp(h)
}
