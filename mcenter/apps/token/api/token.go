package api

import (
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/token"
	"gitee.com/liu-fudan/devcloud-new/mcenter/common/errors"
	"github.com/emicklei/go-restful/v3"
)

// 令牌颁发
func (h *handler) IssueToken(r *restful.Request, w *restful.Response) {
	// 获取用户参数, 强制 content type 来识别body里面内容的格式
	in := token.NewIssueTokenRequest()
	err := r.ReadEntity(in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}

	tk, err := h.service.IssueToken(r.Request.Context(), in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}

	w.WriteEntity(tk)
}
