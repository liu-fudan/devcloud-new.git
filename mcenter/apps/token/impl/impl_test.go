package impl_test

import (
	"context"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/token"
	"gitee.com/liu-fudan/devcloud-new/mcenter/test/tools"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
)

var (
	impl token.Service
	ctx  = context.Background()
)

func init() {
	tools.DevelopmentSetup()

	impl = app.GetInternalApp(token.AppName).(token.Service)
}
