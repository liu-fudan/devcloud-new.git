package impl

import (
	"context"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/token"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/token/provider"
	"go.mongodb.org/mongo-driver/bson"
)

func (i *impl) IssueToken(ctx context.Context, in *token.IssueTokenRequest) (*token.Token, error) {
	if err := in.Validate(); err != nil {
		return nil, err
	}

	issuer := provider.Get(in.GrantType)
	ins, err := issuer.IssueToken(ctx, in)
	if err != nil {
		return nil, err
	}

	_, err = i.col.InsertOne(ctx, ins)
	if err != nil {
		return nil, err
	}
	return ins, nil
}

func (i *impl) ValidateToken(ctx context.Context, in *token.ValidateTokenRequest) (*token.Token, error) {
	tk := token.NewToken()

	err := i.col.FindOne(ctx, bson.M{"_id": in.AccessToken}).Decode(tk)
	if err != nil {
		return nil, err
	}
	err = tk.CheckAliable()
	if err != nil {
		return nil, err
	}
	return tk, nil
}
