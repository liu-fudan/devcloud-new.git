package impl_test

import (
	"testing"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/token"
)

func TestIssueToken(t *testing.T) {
	req := &token.IssueTokenRequest{
		Username: "admin",
		Password: "123456",
	}
	tk, err := impl.IssueToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log((tk))

}

func TestValidateToken(t *testing.T) {
	req := token.NewValidateTokenRequest("cogantqmpgsli31h3f90")
	tk, err := impl.ValidateToken(ctx, req)
	if err != nil {
		t.Log(err)
		t.Fatal(err)
	}
	t.Log(tk)
}
