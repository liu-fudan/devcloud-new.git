package provider

import "gitee.com/liu-fudan/devcloud-new/mcenter/apps/token"

var store = map[token.GRANT_TYPE]Issuer{}

func Registry(key token.GRANT_TYPE, value Issuer) {
	store[key] = value
}

func Get(key token.GRANT_TYPE) TokenIssuer {
	return store[key]
}

func Init() error {
	for _, v := range store {
		if err := v.Config(); err != nil {
			return err
		}
	}
	return nil
}
