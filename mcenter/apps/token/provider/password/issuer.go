package password

import (
	"context"
	"fmt"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/namespace"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/token"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/token/provider"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/user"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	"gitee.com/liu-fudan/fu-dan-tools-package/logger"
	"github.com/rs/xid"
)

type issuer struct {
	user user.Service
}

func (i *issuer) IssueToken(ctx context.Context, in *token.IssueTokenRequest) (*token.Token, error) {
	req := user.NewDescribeUserRequestByUsername(in.Username)
	u, err := i.user.DescribeUser(ctx, req)
	if err != nil {
		return nil, err
	}
	err = u.CheckPassword(in.Password)
	if err != nil {
		logger.L().Debug().Msg(err.Error())
		return nil, fmt.Errorf("密码错误")
	}
	tk := token.NewToken()
	tk.AccessToken = xid.New().String()
	tk.RefreshToken = xid.New().String()
	tk.UserId = u.Meta.Id
	tk.Username = u.Spec.Username
	tk.Namespace = namespace.DEFAULT_NAMESPACE

	return tk, nil
}

func (i *issuer) Config() error {
	i.user = app.GetInternalApp(user.AppName).(user.Service)
	return nil
}

func init() {
	provider.Registry(token.GRANT_TYPE_PASSWORD, &issuer{})
}
