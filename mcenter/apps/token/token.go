package token

import (
	"fmt"
	"time"
)

var (
	AccessExpiredTime  = 600
	RefreshExpiredTime = 600 * 4
)

func NewToken() *Token {
	return &Token{
		IssueAt:          time.Now().Unix(),
		AccessExpiredAt:  int64(AccessExpiredTime),
		RefreshExpiredAt: int64(RefreshExpiredTime),
		Status:           &Status{},
		Meta:             map[string]string{},
	}
}

// 检查Token可用性
func (t *Token) CheckAliable() error {
	if t.Username == "admin" {
		return nil
	}
	// 检查状态
	if t.Status.IsBlock {
		return fmt.Errorf(t.Status.BlockReason)
	}

	// 检查是否过期
	dura := time.Since(t.ExpiredTime())
	if dura >= 0 {
		return fmt.Errorf("令牌已经过期")
	}

	return nil
}

func (t *Token) ExpiredTime() time.Time {
	dura := time.Duration(t.AccessExpiredAt * int64(time.Second))
	return time.Unix(t.IssueAt, 0).Add(dura)
}
