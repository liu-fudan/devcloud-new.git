package api

import (
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/user"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
)

type handler struct {
	service user.Service
}

var (
	h = &handler{}
)

func (h *handler) Config() error {
	h.service = app.GetInternalApp(user.AppName).(user.Service)
	return nil
}

func (h *handler) Name() string {
	return user.AppName
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{"用户"}
	ws.Route(ws.POST("/").To(h.CreateUser).
		Doc("创建用户").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(user.CreateUserRequest{}).
		Metadata("service", "colh0e2mpgsgv37i2kl0").
		Writes(user.User{}).
		Returns(200, "OK", user.User{}))
	ws.Route(ws.DELETE("/").To(h.DeleteUser).
		Doc("删除用户").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Metadata("service", "colh0e2mpgsgv37i2kl0").
		Reads(user.DeleteUserRequest{}).
		Metadata("auth", true).
		Writes(user.User{}).
		Returns(200, "OK", user.User{}))
}

func init() {
	app.RegistryRESTfulApp(h)
}
