package api

import (
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/user"
	"gitee.com/liu-fudan/devcloud-new/mcenter/common/errors"
	"github.com/emicklei/go-restful/v3"
)

func (h *handler) CreateUser(r *restful.Request, w *restful.Response) {
	in := user.NewCreateUserRequest()
	err := r.ReadEntity(in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}
	user, err := h.service.CreateUser(r.Request.Context(), in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}

	w.WriteEntity(user)
}

func (h *handler) DeleteUser(r *restful.Request, w *restful.Response) {
	in := user.NewDeleteUserRequest()
	err := r.ReadEntity(in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}
	user, err := h.service.DeleteUser(r.Request.Context(), in)
	if err != nil {
		w.WriteEntity(errors.Exception{Code: 400, Message: err.Error()})
		return
	}

	w.WriteEntity(user)
}
