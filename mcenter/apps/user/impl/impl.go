package impl

import (
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/user"
	"gitee.com/liu-fudan/devcloud-new/mcenter/conf"
	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

var svc = &impl{}

// user service 的实例类
// 实例类托管, ioc 抽象到公共代码库管理
type impl struct {
	user.UnimplementedRPCServer
	col *mongo.Collection
}

func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection(user.AppName)

	return nil
}

func (i *impl) Name() string {
	return user.AppName
}

func (i *impl) Registry(server *grpc.Server) {
	user.RegisterRPCServer(server, i)
}

func init() {
	app.RegistryGrpcApp(svc)
	app.RegistryInternalApp(svc)
}
