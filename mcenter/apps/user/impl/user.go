package impl

import (
	"context"
	"errors"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/user"
)

const (
	UserName = "username"
	ID       = "_id"
	Domain   = "domain"
)

func (i *impl) CreateUser(ctx context.Context, in *user.CreateUserRequest) (
	*user.User, error) {
	err := in.Validate()
	if err != nil {
		return nil, err
	}

	ins, err := user.New(in)
	if err != nil {
		return nil, err
	}

	_, err = i.col.InsertOne(ctx, ins)
	if err != nil {
		return nil, err
	}

	return ins, nil
}

// 查询用户列表
func (i *impl) QueryUser(ctx context.Context, in *user.QueryUserRequest) (
	*user.UserSet, error) {
	set := user.NewUserSet()

	// 构造查询条件
	filter := bson.M{}
	if in.Keywords != "" {
		filter[UserName] = bson.M{"$regex": in.Keywords, "$options": "im"} // regex正则查找，im 忽略大小写
	}

	opts := &options.FindOptions{}
	opts.SetLimit(int64(in.Page.PageSize))
	opts.SetSkip(in.Page.ComputeOffset())

	cursor, err := i.col.Find(ctx, filter, opts)
	if err != nil {
		return nil, err
	}

	// 执行查询SQL
	for cursor.Next(ctx) {
		ins := user.NewDefaultUser()
		if err := cursor.Decode(ins); err != nil {
			return nil, err
		}
		ins.Desense()
		set.Add(ins)
	}

	// 统计总数
	set.Total, err = i.col.CountDocuments(ctx, filter)
	if err != nil {
		return nil, err
	}
	return set, nil
}

// 查询用户详情
func (i *impl) DescribeUser(ctx context.Context, in *user.DescribeUserRequest) (
	*user.User, error) {
	// 请求校验
	filer := bson.M{}
	switch in.DescribeBy {
	case user.DESCRIBE_BY_USER_ID:
		filer[ID] = in.DescribeVaule
	case user.DESCRIBE_BY_USERNAME:
		filer[UserName] = in.DescribeVaule
	}

	ins := user.NewDefaultUser()
	err := i.col.FindOne(ctx, filer).Decode(ins)
	if err != nil {
		return nil, err
	}

	return ins, nil
}

// 用户更新
func (i *impl) UpdateUser(ctx context.Context, in *user.UpdateUserRequest) (
	*user.User, error) {
	filter := bson.M{UserName: in.Spec.Username, Domain: in.Spec.Domain}
	ins := user.NewDefaultUser()
	err := i.col.FindOne(ctx, filter).Decode(ins)
	if err != nil {
		return nil, err
	}
	if err = ins.CheckPassword(in.Spec.Password); err != nil {
		return nil, fmt.Errorf("密码错误")
	}
	updateMessage := user.NewCreateUserRequest()
	updateMessage.Username = in.Username
	updateMessage.Password = in.Password

	_, err = i.col.UpdateOne(ctx, filter, updateMessage)
	if err != nil {
		return nil, err
	}
	filter[UserName] = in.Username
	err = i.col.FindOne(ctx, filter).Decode(ins)
	if err != nil {
		return nil, err
	}
	return ins, nil
}

// 用户删除
func (i *impl) DeleteUser(ctx context.Context, in *user.DeleteUserRequest) (
	*user.User, error) {
	filter := bson.M{UserName: in.Spec.Username, Domain: in.Spec.Domain}
	ins := user.NewDefaultUser()
	err := i.col.FindOne(ctx, filter).Decode(ins)
	if err != nil {
		return nil, err
	}
	if ins.CheckPassword(in.Spec.Password) != nil {
		return nil, errors.New("password is not correct")
	}
	i.col.DeleteOne(ctx, filter)
	return ins, nil
}
