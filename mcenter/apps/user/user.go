package user

import (
	"encoding/json"

	"gitee.com/liu-fudan/devcloud-new/mcenter/common/meta"
	"golang.org/x/crypto/bcrypt"
)

func NewUserSet() *UserSet {
	return &UserSet{
		Items: []*User{},
	}
}

func (s *UserSet) Add(item *User) {
	s.Items = append(s.Items, item)
}

func NewDefaultUser() *User {
	return &User{
		Meta: meta.NewMeta(),
		Spec: NewCreateUserRequest(),
	}
}

func (i *User) Desense() {
	i.Spec.Password = "***"
}

func (i *User) CheckPassword(pass string) error {
	return bcrypt.CompareHashAndPassword([]byte(i.Spec.Password), []byte(pass))
}

func New(req *CreateUserRequest) (*User, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}

	if err := req.HashPassword(); err != nil {
		return nil, err
	}

	return &User{
		Meta: meta.NewMeta(),
		Spec: req,
	}, nil
}

func (req *CreateUserRequest) HashPassword() error {
	// 专门用户Password hash的方法
	hp, err := bcrypt.GenerateFromPassword([]byte(req.Password), 10)
	if err != nil {
		return err
	}
	req.Password = string(hp)
	return nil
}

// 实现 type Marshaler interface {MarshalJSON() ([]byte, error)}
// 自定义对象序号化方法，proto buffer生成的meta和spec跟user是并列关系，不需要展示meta和spec字段，
// 而只需要它们里面的字段，即将meta和spec内的字段打平。通过内嵌匿名结构体的方式
func (u *User) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		*meta.Meta
		*CreateUserRequest
	}{u.Meta, u.Spec})
}
