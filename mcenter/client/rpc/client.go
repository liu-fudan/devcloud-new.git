package rpc

import (
	"context"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/endpoint"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/permission"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/service"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/token"
)

type ClientSet struct {
	conf *Config
	conn *grpc.ClientConn
}

func NewClient(conf *Config) (*ClientSet, error) {
	ctx, cancel := context.WithTimeout(context.Background(), conf.Timeout())
	defer cancel()

	conn, err := grpc.DialContext(
		ctx,
		conf.Address, // mcenter Grpc 服务地址
		// 不使用TLS
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		// 传递认证信息,conf需实现PerRPCCredentials接口
		grpc.WithPerRPCCredentials(conf),
	)

	if err != nil {
		return nil, err
	}

	return &ClientSet{
		conf: conf,
		conn: conn,
	}, nil
}

func (c *ClientSet) Token() token.RPCClient {
	return token.NewRPCClient(c.conn)
}

func (c *ClientSet) Endpoint() endpoint.RPCClient {
	return endpoint.NewRPCClient(c.conn)
}

func (c *ClientSet) Permission() permission.RPCClient {
	return permission.NewRPCClient(c.conn)
}

func (c *ClientSet) Service() service.RPCClient {
	return service.NewRPCClient(c.conn)
}
