package rpc_test

import (
	"context"
	"testing"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/endpoint"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/permission"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/token"
	"gitee.com/liu-fudan/devcloud-new/mcenter/client/rpc"
)

var (
	client *rpc.ClientSet
	ctx    = context.Background()
)

func TestValidateToken(t *testing.T) {
	req := token.NewValidateTokenRequest("cosbl62mpgsiv622geig")
	tk, err := client.Token().ValidateToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func TestRegistryEndpoint(t *testing.T) {
	req := endpoint.NewRegistryRequest()
	req.Add(&endpoint.CreateEndpointRequest{ServiceId: "test client"})
	eps, err := client.Endpoint().RegistryEndpoint(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(eps)
}

func TestCheckPermission(t *testing.T) {
	req := permission.NewCheckPermissionRequest()
	req.UserId = "cofmtj2mpgsivj6151qg"
	req.Namespace = "default"
	req.ServiceId = "corjurimpgso852orq3g"
	req.HttpMethod = "POST"
	req.HttpPath = "/maudit/api/v1/audit/"
	pm := client.Permission()
	tk, err := pm.CheckPermission(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func init() {
	conf := rpc.NewDefaultConfig()
	conf.ClientID = "colh0e2mpgsgv37i2klg"
	conf.ClientSecret = "colh0e2mpgsgv37i2km0"
	// 如果认证错误请看protocol里的grpc的中间件
	c, err := rpc.NewClient(conf)
	if err != nil {
		panic(err)
	}
	client = c
}
