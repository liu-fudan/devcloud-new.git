package rpc

import (
	"context"
	"time"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/service"
)

// Config 客户端配置
type Config struct {
	Address string `json:"adress" toml:"adress" yaml:"adress" env:"MCENTER_GRPC_ADDRESS"`
	// 客户端认证
	ClientID     string `json:"client_id" toml:"client_id" yaml:"client_id" env:"MCENTER_CLINET_ID"`
	ClientSecret string `json:"client_secret" toml:"client_secret" yaml:"client_secret" env:"MCENTER_CLIENT_SECRET"`
	// 默认值10秒
	TimeoutSecond uint `json:"timeout_second" toml:"timeout_second" yaml:"timeout_second" env:"MCENTER_GRPC_TIMEOUT_SECOND"`
}

func NewDefaultConfig() *Config {
	return &Config{
		Address:       "localhost:18010",
		TimeoutSecond: 10,
	}
}

func (c *Config) Timeout() time.Duration {
	return time.Second * time.Duration(c.TimeoutSecond)
}

func (c *Config) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{
		service.ClientHeaderKey: c.ClientID,
		service.ClientSecretKey: c.ClientSecret,
	}, nil
}

func (c *Config) RequireTransportSecurity() bool { // 是否开启HTTPS
	return false
}
