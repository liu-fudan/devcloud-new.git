package auth

import (
	"fmt"
	"strings"

	"gitee.com/liu-fudan/fu-dan-tools-package/logger"
	"github.com/emicklei/go-restful/v3"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/endpoint"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/permission"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/token"
	"gitee.com/liu-fudan/devcloud-new/mcenter/client/rpc"
)

type httpAuther struct {
	client *rpc.ClientSet
}

func NewHttpAuther(client *rpc.ClientSet) *httpAuther {
	return &httpAuther{client: client}
}

func (a *httpAuther) AuthFunc(req *restful.Request, resp *restful.Response, next *restful.FilterChain) {
	accessRoute := req.SelectedRoute()
	serviceId := accessRoute.Metadata()["service"]
	if serviceId != nil {
		ep := endpoint.NewEndpoint(serviceId.(string), accessRoute.Method(), accessRoute.Path(), accessRoute.Operation())
		isAuth := accessRoute.Metadata()["auth"]
		if isAuth != nil {
			ep.SetAuth(isAuth.(bool))
		}
		isPerm := accessRoute.Metadata()["perm"]
		if isPerm != nil {
			ep.SetPerm(isPerm.(bool))
		}
		logger.L().Debug().Msgf("endpoint: %v", ep)
		if ep.IsAuth() {
			authHeader := req.HeaderParameter(token.TOKEN_HEADER_KEY)
			tkl := strings.Split(authHeader, " ")
			if len(tkl) != 2 {
				resp.Write([]byte("invalid token"))
				return
			}
			tk := tkl[1]
			logger.L().Debug().Msgf("get toekn: %s", tk)

			tkObj, err := a.client.Token().ValidateToken(req.Request.Context(), token.NewValidateTokenRequest(tk))
			if err != nil {
				fmt.Printf("else service use middleware auth failed: %v\n", err.Error())
				resp.Write([]byte(err.Error()))
				return
			}
			// 放入上下文
			req.SetAttribute(token.ATTRIBUTE_TOKEN_KEY, tkObj)
			// 认证后才有鉴权
			if ep.IsPerm() {
				checkReq := permission.NewCheckPermissionRequest()
				checkReq.UserId = tkObj.UserId
				checkReq.Namespace = tkObj.Namespace
				checkReq.ServiceId = ep.Spec.ServiceId
				checkReq.HttpMethod = ep.Spec.Method
				checkReq.HttpPath = ep.Spec.Path
				logger.L().Debug().Msgf("鉴权req: %v", checkReq)
				chekResp, err := a.client.Permission().CheckPermission(req.Request.Context(), checkReq)
				if err != nil {
					resp.Write([]byte(err.Error()))
					return
				}
				if !chekResp.HasPermisson {
					resp.Write([]byte("no permission"))
					return
				}
			}
		}
	}
	// 交给下个处理
	next.ProcessFilter(req, resp)
}
