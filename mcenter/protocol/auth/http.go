package auth

import (
	"errors"
	"fmt"
	"strings"

	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	"gitee.com/liu-fudan/fu-dan-tools-package/logger"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/http/response"

	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/endpoint"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/token"
)

func NewHttpAuther() *httpAuther {
	return &httpAuther{
		token: app.GetInternalApp(token.AppName).(token.Service),
	}
}

type httpAuther struct {
	token token.Service
}

func (a *httpAuther) AuthFunc(req *restful.Request, resp *restful.Response, next *restful.FilterChain) {
	// pr, pw := io.Pipe()
	// tee := io.TeeReader(req.Request.Body, pw)
	// req.Request.Body = pr
	// go func(){
	// 	body, _ := io.ReadAll(tee)
	// fmt.Println(string(body))
	// pw.Close()
	// }()

	// 权限判断：用户当前访问了哪个接口
	accessRoute := req.SelectedRoute()
	// serviceID 自己想办法补充, 生成service client client_id = service_id
	// 自己掉rpc 查询自己的service id已经service的而其配置
	ep := endpoint.NewEndpoint("", accessRoute.Method(), accessRoute.Path(), accessRoute.Operation())
	fmt.Println("拦截中间件ep:", ep)

	// 补充Meta信息
	isAuth := accessRoute.Metadata()["auth"]
	if isAuth != nil {
		ep.SetAuth(isAuth.(bool))
	}

	fmt.Println(ep)

	// 请求拦截并处理
	// 检查Token令牌 如果不合法

	// 开启认证才 认证
	if ep.IsAuth() {
		// 从Header中获取token
		authHeader := req.HeaderParameter(token.TOKEN_HEADER_KEY)
		tkl := strings.Split(authHeader, " ") // Authorization: breaer xxxx
		if len(tkl) != 2 {
			response.Failed(resp, errors.New("令牌不合法, 格式： Authorization: breaer xxxx"))
			return
		}

		tk := tkl[1]
		logger.L().Debug().Msgf("get token: %s", tk)

		// 检查Token的合法性, 需要通过RPC进行检查
		tkObj, err := a.token.ValidateToken(
			req.Request.Context(),
			token.NewValidateTokenRequest(tk),
		)
		if err != nil {
			retErr := errors.New(fmt.Sprintf("令牌校验不合法, %s", err))
			logger.L().Debug().Msgf("mcenter http auth midddleware failed: %v\n", err.Error())
			resp.Write([]byte(retErr.Error()))
			return
		}

		// 放入上下文
		req.SetAttribute(token.ATTRIBUTE_TOKEN_KEY, tkObj)
	}

	// 交给下个处理
	next.ProcessFilter(req, resp)
}
