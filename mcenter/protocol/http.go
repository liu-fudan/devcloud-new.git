package protocol

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitee.com/liu-fudan/fu-dan-tools-package/app"
	"gitee.com/liu-fudan/fu-dan-tools-package/logger"
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"

	"gitee.com/liu-fudan/devcloud-new/maudit/client/broker/audit"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/endpoint"
	"gitee.com/liu-fudan/devcloud-new/mcenter/apps/endpoint/impl"
	"gitee.com/liu-fudan/devcloud-new/mcenter/conf"
	"gitee.com/liu-fudan/devcloud-new/mcenter/protocol/auth"
	"gitee.com/liu-fudan/devcloud-new/mcenter/swagger"
)

// NewHTTPService 构建函数
func NewHTTPService() *HTTPService {
	r := restful.DefaultContainer // restful root router

	// 解决swagger相关问题
	cors := restful.CrossOriginResourceSharing{
		AllowedHeaders: []string{"*"},
		AllowedDomains: []string{"*"},
		AllowedMethods: []string{"HEAD", "OPTIONS", "GET", "POST", "PUT", "PATCH", "DELETE"},
		CookiesAllowed: false,
		Container:      r,
	}
	r.Filter(cors.Filter)
	r.Filter(auth.NewHttpAuther().AuthFunc)
	auditClient := audit.NewHttpAuther()
	r.Filter(auditClient.AuthFunc)

	server := &http.Server{
		ReadHeaderTimeout: 60 * time.Second,
		ReadTimeout:       60 * time.Second,
		WriteTimeout:      60 * time.Second,
		IdleTimeout:       60 * time.Second,
		MaxHeaderBytes:    1 << 20, // 1M
		Addr:              conf.C().App.HTTP.Addr(),
		Handler:           r,
	}

	return &HTTPService{
		r:      r,
		server: server,
		c:      conf.C(),
	}
}

type HTTPService struct {
	r      *restful.Container
	c      *conf.Config
	server *http.Server
}

// mcenter/api/tokens/v1
// mcenter/api/users/v1
func (s *HTTPService) PathPrefix() string {
	return fmt.Sprintf("/%s/api", s.c.App.Name)
}

// Start 启动服务
func (s *HTTPService) Start() error {
	// 装置子服务路由
	app.LoadRESTfulApp(s.PathPrefix(), s.r)

	// 获取当前已经装载的所有Web Sevice
	es := endpoint.NewRegistryRequest()
	wss := s.r.RegisteredWebServices()
	for i := range wss {
		ws := wss[i]
		// 该web service 的所有路由
		routes := ws.Routes()
		for m := range routes {
			route := routes[m]
			// ep := endpoint.NewEndpoint("", route.Method, route.Path, route.Operation)
			ep := &endpoint.CreateEndpointRequest{
				ServiceId: "colh0e2mpgsgv37i2kl0",
				Method:    route.Method,
				Path:      route.Path,
				Operation: route.Operation,
			}
			auth, perm := route.Metadata["auth"], route.Metadata["perm"]
			if auth != nil {
				ep.Auth = auth.(bool)
			}
			if perm != nil {
				ep.Perm = perm.(bool)
			}
			es.Add(ep)
		}
	}
	if eps, err := impl.Svc.RegistryEndpoint(context.Background(), es); err != nil {
		panic("endpoint registry failed!")
	} else {
		fmt.Println(eps)
	}

	// API Doc
	config := restfulspec.Config{
		WebServices:                   restful.RegisteredWebServices(), // you control what services are visible
		APIPath:                       "/apidocs.json",
		PostBuildSwaggerObjectHandler: swagger.Docs,
		DefinitionNameHandler: func(name string) string {
			if name == "state" || name == "sizeCache" || name == "unknownFields" {
				return ""
			}
			return name
		},
	}
	s.r.Add(restfulspec.NewOpenAPIService(config))
	logger.L().Info().Msgf("Get the API using http://%s%s", s.c.App.HTTP.Addr(), config.APIPath)

	// 启动 HTTP服务
	logger.L().Info().Msgf("HTTP服务启动成功, 监听地址: %s", s.server.Addr)
	if err := s.server.ListenAndServe(); err != nil {
		if err == http.ErrServerClosed {
			logger.L().Info().Msgf("service is stopped")
		}
		return fmt.Errorf("start service error, %s", err.Error())
	}
	return nil
}

// Stop 停止server
func (s *HTTPService) Stop() error {
	logger.L().Info().Msgf("start graceful shutdown")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	// 优雅关闭HTTP服务
	if err := s.server.Shutdown(ctx); err != nil {
		logger.L().Error().Msgf("graceful shutdown timeout, force exit")
	}
	return nil
}
