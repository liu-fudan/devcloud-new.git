package tools

import (
	"encoding/json"
	"io"
	"os"
)

func MustReadJsonFile(filepath string, v any) {
	err := ReadJsonFile(filepath, v)
	if err != nil {
		panic(err)
	}
}

func ReadJsonFile(filepath string, v any) error {
	fd, err := os.Open(filepath)
	if err != nil {
		return err
	}
	defer fd.Close()

	payload, err := io.ReadAll(fd)
	if err != nil {
		return err
	}
	return json.Unmarshal(payload, v)
}
